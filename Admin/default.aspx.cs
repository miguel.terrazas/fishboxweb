﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFBX.Clases;
using MySql.Data.MySqlClient;
using System.Data;

namespace WebFBX.Admin
{
    public partial class _default : System.Web.UI.Page
    {
        Clase_Conexion conexion = new Clase_Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable tabla = new DataTable();
            String sql = "";
            sql = "call ProductoSEO (100005, 'https://www.fishboxchile.cl/Productos/', 'https://www.fishboxchile.cl/productos_img/1151/ATUN.png')";
          
            conexion.conectarWP();
            MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.conWP );
            conexion.cerrar();
            datos.Fill(tabla);
            Label1.Text = "<table border=1><tr>";
            for (int c = 0; c < tabla.Columns.Count; c++)
            {
                Label1.Text += "<th>" + tabla.Columns[c].ColumnName  + "</th>";
            }
            Label1.Text+="</tr>";
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                Label1.Text += "<tr>";
                for (int c=0;c<tabla.Columns.Count;c++){
                    Label1.Text += "<td>" + tabla.Rows[i][c] + "</td>";
                }
                Label1.Text += "</tr>";              
            }
        }
    }
}