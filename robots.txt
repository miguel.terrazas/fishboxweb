User-agent: *
Disallow: /Administracion/
Allow: /Administracion/VerProductos.cshtml
Disallow: /card/
Disallow: /img/
Disallow: /temp/
Disallow: /productos_img/
Disallow: /*.pdf$