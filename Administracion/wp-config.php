<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'dolibarrmysql' );

/** MySQL database password */
define( 'DB_PASSWORD', 'd0xed0xe' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'q)`LTm1LTa>;O&~]L]*RU8P&*)WpnEiA5>]A{pP%XBfU-rz#u;7P.@XN8m;GjB,#' );
define( 'SECURE_AUTH_KEY',  'nt w(-Nk*PUPL(T<kJwr6?!M/0Z9l.~X0{:Q6~as7IUjr/(l$PGL3)lSI?5MSWW/' );
define( 'LOGGED_IN_KEY',    '@?E<n)I@K7)oRqA=[-%(_>}wTAdL(yKj jyjV6j(,oJ6$IPFAfT+t[T#*cz~?dls' );
define( 'NONCE_KEY',        '@jnNR(t,8*|aOn=5A V$@5vD)` DYBUDa#hR8.P#7|mw~L@4Wg7y-ZN6`V;9FDRD' );
define( 'AUTH_SALT',        'F*0;ksW6YWEP~/g%TmI,AP_QJlW|q!N{G1c AuZ13U@jhQoxf#PENx&mSI/,KB(?' );
define( 'SECURE_AUTH_SALT', '}^3VE^tQy8fx nV43E&tGDYL[mi3Egv9 5pi3x]T>b|&y:TIfhqALE7_b=FLph+(' );
define( 'LOGGED_IN_SALT',   '/dZL^;W|DHV5m!/dEw=kYM~0Po&GXN`}a{rx@wcT%[/?RKom&<Gf[4.^IRX,h7yj' );
define( 'NONCE_SALT',       '.q#-1X-5IqypID0Z;qme3kOeqMr^p%? U6WMOTZwjC!:lJ}6mcYJ{&~J}&mOx3cZ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
