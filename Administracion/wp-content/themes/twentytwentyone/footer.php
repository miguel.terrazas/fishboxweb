<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	

</div><!-- #page -->

<footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="https://www.fishboxchile.cl/img/favicon.jpg" alt="" style="max-width: 60%;">
                                </a>
                            </div>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-whatsapp"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 offset-xl-1 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                CONTACTOS
                            </h3>
                            <ul>
                                <li><a href="#"><i class="fas fa-location-arrow"></i> Americo Vespucio 1500. Santiago</a></li>
                                <li><a href="#"><i class="fas fa-mobile"></i> Teléfono: +56 9 9879 1040</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                CATEGORIAS
                            </h3>
                            <ul>
                                <li><a href="#">Congelados</a></li>
                                <li><a href="#">Mariscos</a></li>
                                <li><a href="#">Ofertas especiales</a></li>
                                <li><a href="#">Pescados</a></li>
                                <li><a href="#">sin categoria</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                INFORMACIÓN LEGAL
                            </h3>
                            <ul>
                                <li><a href="https://www.fishboxchile.cl/InfoLegal/politica_de_privacidad">> Politicas de Privacidad y Entrega</a></li>
                                <li><a href="https://www.fishboxchile.cl/InfoLegal/terminos_y_condiciones">> Términos y Condiciones</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Esta pagina fue realizada por <a href="https://fishboxchile.cl/" target="_blank">FISHBOXCHILE</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end  -->
</body>
</html>
