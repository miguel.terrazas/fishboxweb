<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> <?php twentytwentyone_the_html_classes(); ?>>
<head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="msapplication-TileImage" content="http://www.fishboxchile.cl/uploads/cropped-favicon-270x270.jpg">

    <meta name="title" content="Fishbox Chile - Caja de Pescados Mariscos a Domicilio">
    <meta name="description" content="Pesca del día, producto fresco directo a su casa, Atún, Salmón, Mariscos, Ostras. Despacho a domicilio. Compre directo whatsapp +56 9 9879 1040">
    <meta name="keywords" content="Atun, Atún, Reineta, congrio, Salmón, Bacalao, lisa, corvina, robalo, atún, atun, merluza, ERIZO, loco cocido, marisco, cholga, ostion, ostras frescas, ostras, CHORO MALTON FRESCO ENTERO, MACHA FRESCA ENTERA, PIURE BOLSA DE 250 GRAMOS, CAMARON IQF CRUDO, CAMARON, LANGOSTA,LANGOSTA ENTERA, PULPO FRESCO, PULPO,  CARNE DE CENTOLLA, CENTOLLA, JAIBA PINZA COCKTAIL">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="Spanish">
    <meta name="revisit-after" content="30 days">

    <link rel="shortcut icon" type="image/x-icon" href="https://www.fishboxchile.cl/img/favicon.png">
    <title>Pesca del día - Productos Frescos</title>
    <link href="https://www.fishboxchile.cl/Content/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/magnific-popup.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/themify-icons.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/nice-select.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/flaticon.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/gijgo.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/slicknav.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/style.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/css/styles_counter.css" rel="stylesheet"/>

    <link href="https://www.fishboxchile.cl/Content/fontawesome-all.css" rel="stylesheet"/>


    <link href="https://www.fishboxchile.cl/Content/alert_message.css" rel="stylesheet"/>
<link href="https://www.fishboxchile.cl/Content/bootstrap-dialog.min.css" rel="stylesheet"/>

    <link href="https://www.fishboxchile.cl/Content/validationEngine.jquery.css" rel="stylesheet"/>

    <link href="https://www.fishboxchile.cl/Content/jquery-ui.css" rel="stylesheet"/>

    <link href="https://www.fishboxchile.cl/css/smoothproducts.css" rel="stylesheet"/>

    <link href="https://www.fishboxchile.cl/css/floating-wpp.min.css" rel="stylesheet"/>

    <script src="https://www.fishboxchile.cl/Scripts/modernizr-2.8.3.js"></script>


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	
	
    <header>
        <div class="header-area ">
            <div class="header-top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-md-6 ">
                            <div class="social_media_links">
                               
                                <div class="titulo_superior_izquierda">
                                    DESPACHAMOS AL DÍA SIGUIENTE TU COMPRA
                                </div>


                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <div class="short_contact_list">
                                <ul>
                                    <li class="contacto"><a href="https://wa.me/56998791040" target="_blank">Contáctanos <i class="fab fa-whatsapp" style="font-size:17px !important"></i>&nbsp; +56 9 9879 1040 </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area main-header-area_movil">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-2">
                            <div class="logo text-center">
                                <a href="/">
                                    <img src="https://www.fishboxchile.cl/img/logo.png" alt="" class="logo_pc">
                                    <img src="https://www.fishboxchile.cl/img/logo-blanco.png" class="logo_movil" alt="" style="max-height:60px;"><span class="logo_movil" style="color:#ffffff">PESCA DEL DÍA</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">

                                        <li><a href="/">TIENDA</a></li>

                                        

                                        <li><a href="#">HORECA</a></li>
                                        <li><a href="https://www.fishboxchile.cl/Contacto/ContactoIndex">CONTACTO</a></li>
                                        <li id="menu_ingresar"><a href="#" id="a_modal_ingresar" class="a_modal_ingresar">INGRESAR</a></li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                            <div class="Appointment">
                                <div class="book_btn d-none d-lg-block">
                                    <a class="" href="#test-form" id="btn_modal_ingresar">Ingresar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"><br /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

	
<div id="page" class="site">
	

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
