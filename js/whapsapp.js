﻿$(function () {
    $('#WAButton').floatingWhatsApp({
        phone: '+56 9 9879 1040', //WhatsApp Business phone number International format-
        headerTitle: '¡Chatea con nosotros en WhatsApp!', //Popup Title
        popupMessage: '¡Hola! Haga clic en uno de nuestros representantes a continuación y nos pondremos en contacto con usted lo antes posible.', //Popup Message
        showPopup: true, //Enables popup display
        buttonImage: '<img src="/../../img/whatsapp.svg" />', //Button Image
        //headerColor: 'crimson', //Custom header color
        //backgroundColor: 'crimson', //Custom background button color
        position: "left"
    });
});