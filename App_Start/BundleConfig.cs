﻿using System.Web;
using System.Web.Optimization;

namespace WebFBX
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/alerta_mensaje").Include(
                "~/Scripts/alert_message.js", "~/Scripts/bootstrap-dialog.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/librerias").Include(
                      "~/js/owl.carousel.min.js",
                      "~/js/isotope.pkgd.min.js",
                      "~/js/ajax-form.js",
                      "~/js/jquery.counterup.min.js",
                      "~/js/imagesloaded.pkgd.min.js",
                      "~/js/scrollIt.js",
                      "~/js/jquery.scrollUp.min.js",
                      "~/js/wow.min.js",
                      "~/js/nice-select.min.js",
                      "~/js/jquery.slicknav.min.js",
                      "~/js/jquery.magnific-popup.min.js",
                      "~/js/plugins.js",
                      "~/js/gijgo.min.js",
                      "~/js/jquery.ajaxchimp.min.js",
                      "~/js/jquery.form.js",
                      "~/js/mail-script.js",
                      "~/js/main.js",
                      "~/js/mail-script.js",
                      "~/js/countdown.js"));

            bundles.Add(new ScriptBundle("~/bundles/whatsapp").Include(
                 "~/js/floating-wpp.min.js", "~/js/whapsapp.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                               "~/Scripts/DataTables/jquery.dataTables.js",
                               "~/Scripts/DataTables/dataTables.bootstrap4.js",
                               "~/Scripts/DataTables/dataTables.responsive.js",
                               "~/Scripts/DataTables/responsive.bootstrap4.js"));


            bundles.Add(new ScriptBundle("~/bundles/funciones").Include(
            "~/Scripts/validador/funciones.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/acceso").Include(
            "~/Scripts/Eventos/acceso.js"));

            bundles.Add(new ScriptBundle("~/bundles/spinner").Include(
            "~/Scripts/Eventos/Spinner.js"));

            bundles.Add(new ScriptBundle("~/bundles/carrito").Include(
            "~/Scripts/Eventos/carrito.js"));

            bundles.Add(new ScriptBundle("~/bundles/fotoProducto").Include(
            "~/Scripts/Eventos/fotosProducto.js"));

            bundles.Add(new ScriptBundle("~/bundles/smoothproducts").Include(
            "~/js/smoothproducts.min.js"));



            bundles.Add(new ScriptBundle("~/bundles/jquery_engine").Include(
            "~/Scripts/validador/languages/jquery.validationEngine-es.js",
            "~/Scripts/validador/jquery.validationEngine.js"));

            bundles.Add(new ScriptBundle("~/bundles/exportarDataTable").Include(
            "~/js/dataTables.buttons.min.js",
            "~/js/buttons.flash.min.js",
            "~/js/jszip.min.js",
            "~/js/pdfmake.min.js",
            "~/js/vfs_fonts.js",
            "~/js/buttons.html5.min.js",
            "~/js/buttons.print.min.js"));

            bundles.Add(new StyleBundle("~/bundles/estilos").Include(
                      "~/Content/bootstrap.min.css",
                      "~/css/magnific-popup.css",
                      "~/css/themify-icons.css",
                      "~/css/nice-select.css",
                      "~/css/flaticon.css",
                      "~/css/gijgo.css",
                      "~/css/slicknav.css",
                      "~/css/slicknav.css",
                      "~/css/style.css",
                      "~/css/styles_counter.css"));
            bundles.Add(new StyleBundle("~/Content/font_awesome").Include("~/Content/fontawesome-all.css", new CssRewriteUrlTransform()));



            bundles.Add(new StyleBundle("~/Content/alerta_mensaje").Include(
            "~/Content/alert_message.css",
            "~/Content/bootstrap-dialog.min.css"));


            bundles.Add(new StyleBundle("~/Content/validationEngine").Include(
            "~/Content/validationEngine.jquery.css"));

            bundles.Add(new StyleBundle("~/estilo/whapsapp").Include(
"~/css/floating-wpp.min.css"));

            bundles.Add(new StyleBundle("~/estilo/datatable").Include(
            "~/Content/DataTables/css/dataTables.bootstrap4.min.css"));
            bundles.Add(new StyleBundle("~/Content/jquery-ui").Include(
            "~/Content/jquery-ui.css"));


            bundles.Add(new StyleBundle("~/estilo/exportarDataTable").Include(
            "~/css/buttons.dataTables.min.css"));


            bundles.Add(new StyleBundle("~/estilo/smoothproductsfoto").Include(
            "~/css/smoothproducts.css"));


        }
    }
}
