﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class DatosClientesModels
    {
        public string rut { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string ciudad { get; set; }
        public string comuna { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public string total { get; set; }
        public int am { get; set; }
        public int pm { get; set; }
        public string condominio { get; set; }
        public double pesototal { get; set; }
        public int inddirecciondiferente { get; set; }
        public string nombredirefente { get; set; }
        public string direcciondirefente { get; set; }
        public string comunadiferente { get; set; }
        public string vendedor { get; set; }

    }
}