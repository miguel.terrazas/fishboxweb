﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class ClientesEsperaModels
    {
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string fecha { get; set; }
        public string email { get; set; }
        public string codigopro { get; set; }
        public string nombreproducto { get; set; }
    }
}