﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class FotosModels
    {
        public string producto { get; set; }
        public string nombre { get; set; }
        public int portada { get; set; }
        public byte[] foto { get; set; }
        public long referencia { get; set; }
        public string formato { get; set; }
        public string rutafoto { get; set; }

    }
}