﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class ProductosEsperaModels
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public int clientes { get; set; }
        public int agotado { get; set; }

    }
}