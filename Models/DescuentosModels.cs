﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class DescuentosModels
    {
        public string codigo { get; set; }
        public double valor { get; set; }
        public int cantidad { get; set; }
        public int consumido { get; set; }
        public int tipo { get; set; }
    }
}