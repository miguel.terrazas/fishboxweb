﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class FamiliasModels
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string referencia { get; set; }
        public byte[] foto { get; set; }


    }
}