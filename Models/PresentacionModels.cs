﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class PresentacionModels
    {
        public string codigo { get; set; }
        public string codpre { get; set; }
        public string nombre { get; set; }
    }
}