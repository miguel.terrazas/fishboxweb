﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Models
{
    public class ProductosModels
    {
        public string id_indice { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string unidad { get; set; }
        public string observaciones { get; set; }
        public string pesogrs { get; set; }
        public string marca { get; set; }
        public string tipo { get; set; }
        public int precio { get; set; }
        public int costo { get; set; }
        public int preciooriginal { get; set; }
        public double stock { get; set; }
        public int agotado { get; set; }
        public string venceoferta { get; set; }
        public int oferta { get; set; }
        public double preciooferta { get; set; }
        public double cantidad { get; set; }
        public int ofertavigente { get; set; }
        public int valor { get; set; }
        public int indpagweb { get; set; }
        public string nombrecorto { get; set; }
        public string nfamilia { get; set; }
        public int familia { get; set; }
        public string rutafoto { get; set; }
        public int indcabezaesquelon { get; set; }
        public int presentacion { get; set; }
        public int cabeza { get; set; }
        public int esquelon { get; set; }
        public int indpresentacion { get; set; }
        public int inddescripcion { get; set; }
        public string titulodescripcion { get; set; }
        public string descripcion { get; set; }
        public string titulovideo { get; set; }
        public string codigovideo1 { get; set; }
        public string codigovideo2 { get; set; }
        public double cantidaddesde { get; set; }


    }
}