﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace WebFBX.Clases
{
    public class EnvioCorreo
    {

        public Respuesta enviaCorreo(string email_cliente, string email_envio, string passEnvio, string smtp, int puerto, string nropedido,
           string nombrecliente, string emaildestinatario, DataTable productos, DataTable Direccioncliente)
        {
            Respuesta resp = new Respuesta();
            try
            {
                string titulo_subjet_contacto = "[Pesca del día] Nuevo pedido #(" + nropedido + ")";
                DateTime fecha_actual = DateTime.Now;
                string mes_str = MonthName(fecha_actual.Month);
                string fecha = fecha_actual.Day.ToString() + " de " + mes_str + " del " + fecha_actual.Year.ToString();
                double precio_item = 0;
                double valor_despacho = 0;
                double total = 0;
                double descuento = 0;
                var folder = HttpContext.Current.Server.MapPath("~/img/favicon.png");
                var folder_transferencia = HttpContext.Current.Server.MapPath("~/img/transferencia.jpg");



                string body = "<center><IMG id='fbx' border='0' alt='fbx' src='cid:favicon' width='94' height='94'></center>";
                body = body + "<p>Has recibido el siguiente pedido de : " + nombrecliente + "</p>";
                body = body + "<p>[Pedido #" + nropedido + "] (" + fecha + ")</p>";
                body = body + "<p></p>";
                body = body + "<table width='100%' style='border:1px solid black;border-collapse:collapse;'><tr><th style='background-color:#1d0d9b;color:#fff'>Producto</th><th style='background-color:#1d0d9b;color:#fff'>Cantidad</th><th  style='background-color:#1d0d9b;color:#fff'>Precio</th></tr>";
                foreach (DataRow row in productos.Rows)
                {
                    string codigo = row["codigo"].ToString();
                    if (codigo != "405" & codigo != "406")
                    {
                        string producto = row["detalle"].ToString();
                        string cantidad = row["cantidad"].ToString();
                        string precio = "$" + Convert.ToDouble(row["precio"]).ToString("N0");
                        precio_item = precio_item + Convert.ToDouble(row["precio"]) * Convert.ToDouble(row["cantidad"]);
                        body = body + "<tr><td style='border:1px solid black;'>" + producto + "</td><td style='border:1px solid black;'>" + cantidad + "</td><td style='border:1px solid black;'>" + precio + "</td></tr>";
                    }
                    else if (codigo == "406")
                    {
                        descuento = Convert.ToDouble(row["precio"]);
                    }
                    else
                    {
                        valor_despacho = Convert.ToDouble(row["precio"]);
                    }
                }
                total = valor_despacho + precio_item + descuento;
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Subtotal:</td><td style='border:1px solid black;'>$" + precio_item.ToString("N0") + "</td></tr>";
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Envío:</td><td style='border:1px solid black;'>$" + valor_despacho.ToString("N0") + " Vía cobro por despacho</td></tr>";
                if (descuento < 0)
                {
                    body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Descuento:</td><td style='border:1px solid black;'>$" + descuento.ToString("N0") + "</td></tr>";

                }
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Total:</td><td style='border:1px solid black;'>$" + total.ToString("N0") + "</td></tr>";
                body = body + "</table>";
                body = body + "<p></p>";
                body = body + "<table width='100%' style='border:1px solid black;border-collapse:collapse;'><tr><th style='background-color:#1d0d9b;color:#fff' ><b>Dirección de cliente</b></th><th style='background-color:#1d0d9b;color:#fff'><b>Dirección de envío</b></th></tr>";



                foreach (DataRow row in Direccioncliente.Rows)
                {
                    string direccionenvio = row["direccionenvio"].ToString();
                    string comunaenvio = row["comunaenvio"].ToString();

                    string direccioncliente = row["direccioncliente"].ToString();
                    string comuna = row["comuna"].ToString();
                    string telefono = row["telefono"].ToString();
                    string email = "<a href='mailto:" + row["email"].ToString() + "' target ='_blank'><span style='color:#0563C1'>" + row["email"].ToString() + "</span></a>";

                    body = body + "<tr><td style='border:1px solid black;'>" + nombrecliente + "<br />" +
                        direccioncliente + "<br />" + comuna + direccionenvio + "<br />" + "<u>" +
                        telefono + "</u><br />" + email + "</td><td style='border:1px solid black;'>" + nombrecliente + "<br />" + direccionenvio + "<br />" + comunaenvio + "</td></tr>";
                }
                body = body + "</table>";

                body = body + "<br/>Felicidades por su pedido<br/>";
                string imagePathF = folder;//aqui la ruta de tu imagen

                SmtpClient client = new SmtpClient
                {
                    Host = smtp,
                    Port = puerto,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(email_envio, passEnvio)
                };
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(email_envio),
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = true,
                    Priority = MailPriority.Normal,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    Subject = titulo_subjet_contacto,
                    Body = body,
                };


                mail.Bcc.Add(emaildestinatario);//revisador pedido
                mail.To.Add(new MailAddress(email_cliente));
                Attachment attachment = new Attachment(folder);
                mail.Attachments.Add(attachment);

                mail.Attachments.Add(new Attachment(folder_transferencia));

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object s
                        , X509Certificate certificate
                        , X509Chain chain
                        , SslPolicyErrors sslPolicyErrors)
                    { return true; };

                client.Send(mail);



                resp.retorno = 1;
                resp.mensaje = "Correo enviado exitosamente";

            }
            catch (Exception ex)
            {
                resp.retorno = -3;
                resp.mensaje = ex.Message;

            }


            return resp;
        }

        public Respuesta enviaCorreo_0(string email_cliente, string email_envio, string passEnvio, string smtp, int puerto, string nropedido,
            string nombrecliente, string emaildestinatario, DataTable productos, DataTable Direccioncliente)
        {
            Respuesta resp = new Respuesta();
            try
            {
                string titulo_subjet_contacto = "[Pesca del día] Nuevo pedido #(" + nropedido + ")";
                DateTime fecha_actual = DateTime.Now;
                string mes_str = MonthName(fecha_actual.Month);
                string fecha = fecha_actual.Day.ToString() + " de " + mes_str + " del " + fecha_actual.Year.ToString();
                double precio_item = 0;
                double valor_despacho = 0;
                double total = 0;
                double descuento = 0;
                var folder = HttpContext.Current.Server.MapPath("~/img/favicon.png");
                var folder_transferencia = HttpContext.Current.Server.MapPath("~/img/transferencia.jpg");



                string body = "<center><IMG id='fbx' border='0' alt='fbx' src='cid:fbx' width='94' height='94'></center>";
                body = body + "<p>Has recibido el siguiente pedido de : " + nombrecliente + "</p>";
                body = body + "<p>[Pedido #" + nropedido + "] (" + fecha + ")</p>";
                body = body + "<p></p>";
                body = body + "<table width='100%' style='border:1px solid black;border-collapse:collapse;'><tr><th style='background-color:#1d0d9b;color:#fff'>Producto</th><th style='background-color:#1d0d9b;color:#fff'>Cantidad</th><th  style='background-color:#1d0d9b;color:#fff'>Precio</th></tr>";
                foreach (DataRow row in productos.Rows)
                {
                    string codigo = row["codigo"].ToString();
                    if (codigo != "405" & codigo!="406")
                    {
                        string producto = row["detalle"].ToString();
                        string cantidad = row["cantidad"].ToString();
                        string precio = "$" + Convert.ToDouble(row["precio"]).ToString("N0");
                        precio_item = precio_item + Convert.ToDouble(row["precio"])* Convert.ToDouble(row["cantidad"]);
                        body = body + "<tr><td style='border:1px solid black;'>" + producto + "</td><td style='border:1px solid black;'>" + cantidad + "</td><td style='border:1px solid black;'>" + precio + "</td></tr>";
                    }else if(codigo == "406")
                    {
                        descuento = Convert.ToDouble(row["precio"]);
                    }
                    else
                    {
                        valor_despacho = Convert.ToDouble(row["precio"]);
                    }
                }
                total = valor_despacho + precio_item + descuento;
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Subtotal:</td><td style='border:1px solid black;'>$" + precio_item.ToString("N0") + "</td></tr>";
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Envío:</td><td style='border:1px solid black;'>$" + valor_despacho.ToString("N0") + " Vía cobro por despacho</td></tr>";
                if (descuento < 0)
                {
                    body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Descuento:</td><td style='border:1px solid black;'>$" + descuento.ToString("N0") + "</td></tr>";

                }
                body = body + "<tr><td colspan='2' style='border:1px solid black;text-align:right'>Total:</td><td style='border:1px solid black;'>$" + total.ToString("N0") + "</td></tr>";
                body = body + "</table>";
                body = body + "<p></p>";
                body = body + "<table width='100%' style='border:1px solid black;border-collapse:collapse;'><tr><th style='background-color:#1d0d9b;color:#fff' ><b>Dirección de cliente</b></th><th style='background-color:#1d0d9b;color:#fff'><b>Dirección de envío</b></th></tr>";



                foreach (DataRow row in Direccioncliente.Rows)
                {
                    string direccionenvio = row["direccionenvio"].ToString();
                    string comunaenvio = row["comunaenvio"].ToString();

                    string direccioncliente = row["direccioncliente"].ToString();
                    string comuna = row["comuna"].ToString();
                    string telefono = row["telefono"].ToString();
                    string email = "<a href='mailto:"+ row["email"].ToString()+"' target ='_blank'><span style='color:#0563C1'>"+ row["email"].ToString() +"</span></a>";   

                    body = body + "<tr><td style='border:1px solid black;'>" + nombrecliente + "<br />"+
                        direccioncliente +"<br />"+ comuna  + direccionenvio +"<br />"+"<u>" +
                        telefono+"</u><br />"+ email + "</td><td style='border:1px solid black;'>" + nombrecliente +"<br />"+direccionenvio + "<br />"+ comunaenvio +"</td></tr>";
                }
                body = body + "</table>";

                body = body + "<br/>Felicidades por su pedido<br/>";
                body = body + "<br/><IMG id='transferencia' border='0' alt='fbx' src='cid:transferencia'><br/>";

                MailMessage mensaje = new MailMessage(email_cliente, email_cliente);
                mensaje.Subject = titulo_subjet_contacto;
                mensaje.Bcc.Add(emaildestinatario);//revisador pedido
                string textBody = body;
                AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(textBody, null, MediaTypeNames.Text.Plain);

                string htmlBody = "<html><body>" + textBody + "</body></html>";
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

                string imagePathF =folder;//aqui la ruta de tu imagen
                LinkedResource face = new LinkedResource(imagePathF);
                face.ContentId = "fbx";
                htmlView.LinkedResources.Add(face);

                string imagePathTra = folder_transferencia;//aqui la ruta de tu imagen
                LinkedResource transf = new LinkedResource(imagePathTra);
                transf.ContentId = "transferencia";
                htmlView.LinkedResources.Add(transf);
                mensaje.AlternateViews.Add(htmlView);

                // Definir el servidor SMTP, GMail usa SSL para la autenticación
                SmtpClient smtp_host = new SmtpClient(smtp, puerto);
                smtp_host.EnableSsl = true;

                // Ingresar nuestra cuenta de gmail
                smtp_host.Credentials = new NetworkCredential(email_envio, passEnvio);
                smtp_host.Send(mensaje);
                //SmtpClient client = new SmtpClient
                //{
                //    Host = smtp,
                //    Port = puerto,
                //    EnableSsl = true,
                //    Timeout = 10000,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(email_envio, passEnvio)
                //};
                //MailMessage mail = new MailMessage
                //{
                //    From = new MailAddress(email_envio),
                //    BodyEncoding = Encoding.UTF8,
                //    IsBodyHtml = true,
                //    Priority = MailPriority.Normal,
                //    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                //    Subject = titulo_subjet_contacto,
                //    Body = body,
                //};


                //mail.To.Add(new MailAddress(emaildestinatario));
                //ServicePointManager.ServerCertificateValidationCallback =
                //    delegate (object s
                //        , X509Certificate certificate
                //        , X509Chain chain
                //        , SslPolicyErrors sslPolicyErrors)
                //    { return true; };

                //client.Send(mail);
                resp.retorno = 1;
                resp.mensaje = "Correo enviado exitosamente";

            }
            catch (Exception ex)
            {
                resp.retorno = -3;
                resp.mensaje = ex.Message;

            }


            return resp;
        }

        public string MonthName(int month)
        {
            DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
            return dtinfo.GetMonthName(month);
        }
    }
}