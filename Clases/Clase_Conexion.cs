﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WebFBX.Clases
{
    public class Clase_Conexion
    {
        public static string servidor = "localhost";
        public static string usuario_server = "fishbox_1";
        public static string password_server = "fishbox_1";
        public static string bd = "webbox";
        public static string puerto = "3306";

        /*public static string servidor = "localhost";///"opencode01.sytes.net";
        public static string usuario_server = "conta";
        public static string password_server = "conta";
        public static string bd = "webbox";
        public static string puerto = "3306";
        */

        public static string servidorWP = "45.239.111.90";
        public static string usuario_serverWP = "wordpres_useradmin";
        public static string password_serverWP = "Ab135790.";
        public static string bdWP = "wordpres_wp";
        public static string puertoWP = "3306";
        //
        public MySqlConnection con_login;
        public MySqlConnection con_contabilidad;
        public MySqlConnection con;

        public MySqlConnection con_loginWP;
        public MySqlConnection con_contabilidadWP;
        public MySqlConnection conWP;

        public void conectar()
        {

            //con = new MySqlConnection("server=localhost;user id=root;password=micasa1472;database=negocio_vnorte;port=3306;Connect Timeout=200;Allow User Variables=true;CheckParameters=False");
            con = new MySqlConnection("server=" + servidor + "; user id=" + usuario_server + "; password=" + password_server + "; database=" + bd + ";port=" + puerto + ";Connect Timeout=200;Allow User Variables=true;CheckParameters=False");
            con.Open();
        }

        public void conectarWP()
        {
            //con = new MySqlConnection("server=localhost;user id=root;password=micasa1472;database=negocio_vnorte;port=3306;Connect Timeout=200;Allow User Variables=true;CheckParameters=False");
            conWP = new MySqlConnection("server=" + servidorWP + "; user id=" + usuario_serverWP + "; password=" + password_serverWP + "; database=" + bdWP + ";port=" + puertoWP + ";Connect Timeout=200;Allow User Variables=true;CheckParameters=False");
            conWP.Open();
        }
        //metodo para cerrar
        public void cerrar()
        {
            con.Close();
        }
        public void cerrarWP()
        {
            conWP.Close();
        }
    }
}