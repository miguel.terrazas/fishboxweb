﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFBX.Clases
{
    public class Respuesta
    {
        public int retorno { get; set; }
        public string mensaje { get; set; }
    }
}