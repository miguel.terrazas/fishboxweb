﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class PedidosController : Controller
    {
        private Dictionary<string, string> request = new Dictionary<string, string>();
        Dictionary<string, object> objData = new Dictionary<string, object>();

        // GET: Transbank
        Clase_Conexion conexion = new Clase_Conexion();
        public ActionResult GuardarPedidoTransferencia(DatosClientesModels datos_clienes_obj, List<ProductosModels> carrito)
        {
            string[] retorno_pedido = GuardarPedido(datos_clienes_obj, carrito);

            if (retorno_pedido[0] == "-1")
            {
                return Json(new { retornoJson = -2, mensajeJson = retorno_pedido[1] });
            }
            else if (retorno_pedido[0] == "-2")
            {
                return Json(new { retornoJson = -2, mensajeJson = retorno_pedido[1] });
            }
            else if (retorno_pedido[0] == "-3")
            {
                return Json(new { retornoJson = -3, mensajeJson = "Pedido guardado correctemente, pero no ha podido email no ha podido ser enviado:" + retorno_pedido[1] });
            }
            else
            {
                return Json(1);
            }

        }

        public string obtieneUnidad(string codigo)
        {
            string sql = "SELECT unidad FROM productos WHERE codigo=@codigo";
            string unidad = "Un";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        unidad = d["unidad"].ToString();
                    }
                    else
                    {

                    }
                }
            }
            return unidad;
        }

        public int obtieneConsumidoDescuento(string codigo)
        {
            string sql = "SELECT consumido FROM descuentos WHERE coddesc=@codigo FOR UPDATE";
            int consumido = 0;
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        consumido = Convert.ToInt32(d["consumido"]);
                    }
                    else
                    {

                    }
                }
            }
            return consumido;
        }
        public string presentacion(int codigo)
        {
            string sql = "SELECT nombre FROM presentaciones WHERE codigo=@codigo";
            string nombre = "";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);

                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {

                        nombre = d["nombre"].ToString();
                    }
                    else
                    {

                    }
                }
            }
            return nombre;
        }

        // GET: Pedidos
        public string[] GuardarPedido(DatosClientesModels datos_clienes_obj, List<ProductosModels> carrito)
        {
            int retorno = 0;
            String error_msj = "";
            //double pctiva = 19;
            //string nombre_producto = "";

            try
            {
                DataTable lista_tabla = new DataTable();
                DataTable tablaFolio = new DataTable();
                DataTable tablaFolio2 = new DataTable();
                DateTime fecha_dt = DateTime.Now;
                DataTable dt_productos = new DataTable();
                DataTable dt_direccioncliente = new DataTable();
                Respuesta resp = new Respuesta();
                EnvioCorreo env = new EnvioCorreo();
                string nombrepresentacion = "";
                string cabezaesquelon = "";

                int pesototal = 0;
                string fecha = (fecha_dt).ToString("yyyy-MM-dd");
                conexion.conectar();
                String start = "start transaction";
                MySqlCommand datosST = new MySqlCommand(start, conexion.con);
                datosST.ExecuteScalar();

                string email = "prueba.opencode@gmail.com";
                string password = "opencode11";
                string smtp = "smtp.gmail.com";
                int puerto = 25;
                string emailrespuesta = "prueba.opencode@gmail.com";

                string sql = "SELECT email,password,smtp,puerto,emailrespuesta FROM parametros";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {

                            email = d["email"].ToString();
                            password = d["password"].ToString();
                            smtp = d["smtp"].ToString();
                            puerto = Convert.ToInt32(d["puerto"].ToString());
                            emailrespuesta = d["emailrespuesta"].ToString();
                        }
                        else
                        {

                        }
                    }
                }
                if (Session["codigo_descuento"] != null)
                {
                    string codigo_desc = Session["codigo_descuento"].ToString();
                    int consumido = obtieneConsumidoDescuento(codigo_desc);
                    consumido = consumido + 1;
                    String sql_consumido_descuento= " UPDATE descuentos";
                    sql_consumido_descuento += " SET consumido =@consumido ";
                    sql_consumido_descuento += " WHERE coddesc =@coddesc";
                    MySqlCommand datosDesc = new MySqlCommand(sql_consumido_descuento, conexion.con);
                    datosDesc.Parameters.Add(new MySqlParameter("@consumido", consumido));
                    datosDesc.Parameters.Add(new MySqlParameter("@coddesc", codigo_desc));
                    datosDesc.ExecuteNonQuery();
                }
                String sqlPedido = " INSERT pedidos";
                sqlPedido += " SET fecha =@fecha,";
                sqlPedido += " telefono =@telefono,";
                sqlPedido += " cliente =@cliente,";
                sqlPedido += " direccionenvio =@direccionenvio,";
                sqlPedido += " comunaenvio =@comunaenvio,";
                sqlPedido += " condominioenvio =@condominioenvio,";
                sqlPedido += " vendedor =@vendedor,";
                sqlPedido += " am =@am,";
                sqlPedido += " pm =@pm,";
                sqlPedido += " fechaent =@fechaent,";
                sqlPedido += " pesototal =@pesototal,";
                sqlPedido += " total =@total,";
                sqlPedido += " entregaboleta =@entregaboleta,";
                sqlPedido += " web =@web,";
                sqlPedido += " indpago =@indpago";

                string nombre = "";
                string direccion = "";
                string comuna = "";

                if (datos_clienes_obj.inddirecciondiferente == 1)
                {
                    nombre = datos_clienes_obj.nombredirefente;
                    direccion = datos_clienes_obj.direcciondirefente;
                    comuna = datos_clienes_obj.comunadiferente;
                }
                else
                {
                    nombre = datos_clienes_obj.nombre;
                    direccion = datos_clienes_obj.direccion;
                    comuna = datos_clienes_obj.comuna;
                }

                dt_direccioncliente.Columns.Add(new DataColumn("direccionenvio"));
                dt_direccioncliente.Columns.Add(new DataColumn("comunaenvio"));
                dt_direccioncliente.Columns.Add(new DataColumn("direccioncliente"));
                dt_direccioncliente.Columns.Add(new DataColumn("comuna"));
                dt_direccioncliente.Columns.Add(new DataColumn("telefono"));
                dt_direccioncliente.Columns.Add(new DataColumn("email"));

                MySqlCommand datosNV = new MySqlCommand(sqlPedido, conexion.con);
                datosNV.Parameters.Add(new MySqlParameter("@fecha", fecha));
                datosNV.Parameters.Add(new MySqlParameter("@telefono", datos_clienes_obj.telefono.Replace(" ", "")));
                datosNV.Parameters.Add(new MySqlParameter("@cliente", nombre));
                datosNV.Parameters.Add(new MySqlParameter("@direccionenvio", direccion));
                datosNV.Parameters.Add(new MySqlParameter("@comunaenvio", comuna));
                datosNV.Parameters.Add(new MySqlParameter("@condominioenvio", datos_clienes_obj.condominio));
                datosNV.Parameters.Add(new MySqlParameter("@am", datos_clienes_obj.am));
                datosNV.Parameters.Add(new MySqlParameter("@pm", datos_clienes_obj.pm));
                datosNV.Parameters.Add(new MySqlParameter("@vendedor", datos_clienes_obj.vendedor));
                datosNV.Parameters.Add(new MySqlParameter("@entregaboleta", 1));
                datosNV.Parameters.Add(new MySqlParameter("@fechaent", DateTime.Now));
                datosNV.Parameters.Add(new MySqlParameter("@pesototal", datos_clienes_obj.pesototal));
                datosNV.Parameters.Add(new MySqlParameter("@total", datos_clienes_obj.total));
                datosNV.Parameters.Add(new MySqlParameter("@web", 1));
                datosNV.Parameters.Add(new MySqlParameter("@indpago", 1));//1:pago transferencia

                datosNV.ExecuteNonQuery();

                if (datosNV.LastInsertedId != 0) datosNV.Parameters.Add(new MySqlParameter("registro", datosNV.LastInsertedId));
                int lastId_registro = Convert.ToInt32(datosNV.Parameters["@registro"].Value);
                DataRow row_dir = dt_direccioncliente.NewRow();
                row_dir["direccionenvio"] = direccion;
                row_dir["comunaenvio"] = comuna;
                row_dir["direccioncliente"] = datos_clienes_obj.direccion;
                row_dir["comuna"] = datos_clienes_obj.comuna;
                row_dir["telefono"] = datos_clienes_obj.telefono.Replace(" ", "");
                row_dir["email"] = datos_clienes_obj.email;
                dt_direccioncliente.Rows.Add(row_dir);


                int nroitem = 0;
                int existe_stock = 0;
                //double cantidad_stock = 0;
                //double precio_det = 0;
                dt_productos.Columns.Add(new DataColumn("codigo"));
                dt_productos.Columns.Add(new DataColumn("detalle"));
                dt_productos.Columns.Add(new DataColumn("cantidad", typeof(Double)));
                dt_productos.Columns.Add(new DataColumn("precio", typeof(Double)));
                foreach (var detalle in carrito)
                {
                    nombrepresentacion = "";
                    cabezaesquelon = "";
                    nroitem += 1;
                    String sqlPedidoD = " INSERT pedidosd";
                    sqlPedidoD += " SET registro =@registro,";
                    sqlPedidoD += " fecha =@fecha,";
                    sqlPedidoD += " telefono =@telefono,";
                    sqlPedidoD += " cliente =@cliente,";
                    sqlPedidoD += " codigo =@codigo,";
                    sqlPedidoD += " cantidad =@cantidad,";
                    sqlPedidoD += " unidad =@unidad,";
                    sqlPedidoD += " detalle =@detalle,";
                    sqlPedidoD += " precio =@precio,";
                    sqlPedidoD += " cantidadreal =@cantidadreal,";
                    sqlPedidoD += " valor =@valor,";
                    sqlPedidoD += " costo =@costo,";
                    sqlPedidoD += " presentacion =@presentacion,";
                    sqlPedidoD += " esquelon =@esquelon,";
                    sqlPedidoD += " cabeza =@cabeza";


                    int valor = detalle.valor;
                    if (detalle.unidad=="" || detalle.unidad == null)
                    {
                        detalle.unidad = obtieneUnidad(detalle.codigo);
                    }

                    MySqlCommand datosNVD = new MySqlCommand(sqlPedidoD, conexion.con);
                    datosNVD.Parameters.Add(new MySqlParameter("@registro", lastId_registro));
                    datosNVD.Parameters.Add(new MySqlParameter("@fecha", fecha));
                    datosNVD.Parameters.Add(new MySqlParameter("@telefono", datos_clienes_obj.telefono.Replace(" ", "")));
                    datosNVD.Parameters.Add(new MySqlParameter("@cliente", datos_clienes_obj.nombre));
                    datosNVD.Parameters.Add(new MySqlParameter("@codigo", detalle.codigo));
                    datosNVD.Parameters.Add(new MySqlParameter("@cantidad", detalle.cantidad));
                    datosNVD.Parameters.Add(new MySqlParameter("@unidad", detalle.unidad));
                    datosNVD.Parameters.Add(new MySqlParameter("@detalle", detalle.nombre));
                    datosNVD.Parameters.Add(new MySqlParameter("@precio", detalle.precio));
                    datosNVD.Parameters.Add(new MySqlParameter("@cantidadreal", detalle.cantidad));
                    datosNVD.Parameters.Add(new MySqlParameter("@valor", valor));
                    datosNVD.Parameters.Add(new MySqlParameter("@costo", detalle.costo));
                    datosNVD.Parameters.Add(new MySqlParameter("@presentacion", detalle.presentacion));
                    datosNVD.Parameters.Add(new MySqlParameter("@esquelon", detalle.esquelon));
                    datosNVD.Parameters.Add(new MySqlParameter("@cabeza", detalle.cabeza));
                    datosNVD.ExecuteNonQuery();
                    if (detalle.presentacion > 0)
                    {
                        nombrepresentacion = "<br /> TIPO CORTE: " + presentacion(detalle.presentacion);
                    }
                    if (detalle.esquelon > 0 & detalle.cabeza > 0)
                    {
                        cabezaesquelon = "<br />CABEZA/EZQUELON";
                    }
                    else if (detalle.cabeza > 0)
                    {
                        cabezaesquelon = "<br />CABEZA";
                    }
                    else if (detalle.esquelon > 0)
                    {
                        cabezaesquelon = "<br />EZQUELON";
                    }
                    DataRow row = dt_productos.NewRow();
                    row["codigo"] = detalle.codigo;
                    row["detalle"] = detalle.nombre + cabezaesquelon + nombrepresentacion;
                    row["cantidad"] = detalle.cantidad;
                    row["precio"] = detalle.precio;
                    dt_productos.Rows.Add(row);
                }
                Session["carrito"] = null;
                Session["contador"] = 0;
                Session["ss_familia"] = null;
                resp = env.enviaCorreo(datos_clienes_obj.email, email, password, smtp, puerto, lastId_registro.ToString(),
                datos_clienes_obj.nombre, emailrespuesta, dt_productos, dt_direccioncliente);
                error_msj = resp.mensaje;
                if (existe_stock == -2)
                {
                    retorno = existe_stock;
                    String rollback = "rollback";
                    MySqlCommand datosRollback = new MySqlCommand(rollback, conexion.con);
                    datosRollback.ExecuteScalar();
                }
                else
                {
                    if (resp.retorno > 0)
                    {
                        retorno = lastId_registro;
                    }
                    else
                    {
                        retorno = resp.retorno;
                    }
                    String commit = "commit";
                    MySqlCommand datosCommit = new MySqlCommand(commit, conexion.con);
                    datosCommit.ExecuteScalar();
                    Session["codigo_descuento"] = null;
                }

            }
            catch (Exception ex)
            {
                String rollback = "rollback";
                MySqlCommand datosRollback = new MySqlCommand(rollback, conexion.con);
                datosRollback.ExecuteScalar();
                error_msj = ex.Message.ToString().Replace("'", "");
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }

            string[] array = new string[3];
            array[0] = retorno.ToString();
            array[1] = error_msj;
            array[2] = datos_clienes_obj.total;

            return array;
        }
    }
}