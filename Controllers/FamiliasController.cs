﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;
using System.IO;
using System.Text;

namespace WebFBX.Controllers
{
    public class FamiliasController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        [HttpPost]
        public ActionResult partialFoto(string referencia)
        {
            ViewData["referencia"] = referencia;
            return View();
        }

        public ActionResult GetFoto(int referencia)
        {
            try
            {
                conexion.conectar();
                string sql = "SELECT foto,formato AS extension FROM familias WHERE referencia=" + referencia + "  ";
                byte[] imagen = null;
                string extension = "";
                string nombre = "foto";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            string f = d["foto"].ToString();
                            if (f == "")
                            {
                                var folder = Server.MapPath("~/img/imagen_sin_foto.png");
                                extension = "image/png";
                                imagen = System.IO.File.ReadAllBytes(folder);
                            }
                            else
                            {
                                imagen = (byte[])d["foto"];
                                extension = (string)d["extension"];
                            }
                            nombre = nombre + referencia.ToString() + "." + extension;
                        }
                        else
                        {
                            var folder = Server.MapPath("~/img/imagen_sin_foto.png");
                            string contentType = "image/png";
                            byte[] foto_blanco = System.IO.File.ReadAllBytes(folder);
                            //MySqlConnection.ClearAllPools();
                            return File(foto_blanco, contentType, "imagen.png");
                        }
                    }
                }
                //return File(imagen, "image/jpeg");
                return File(imagen, "application/octet-stream", nombre);

            }
            catch (Exception ex)
            {
                string erro = ex.Message;
                return Json(ex);
            }
            finally
            {
                conexion.cerrar();

            }
        }

        [HttpPost]
        public ActionResult EditarFotoFamilia(string referencia)
        {
            var retorno = "0";
            try
            {

                conexion.conectar();

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    if (file.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        string formato = System.IO.Path.GetExtension(file.FileName);

                        using (BinaryReader reader = new BinaryReader(file.InputStream))
                        {
                            byte[] archivo_binario = reader.ReadBytes(file.ContentLength);

                            string sql = "UPDATE familias ";
                            sql += "SET foto = @foto";
                            sql += ",formato = @formato";
                            sql += " WHERE referencia = @referencia";



                            MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                            datos.Parameters.Add(new MySqlParameter("@foto", archivo_binario));
                            datos.Parameters.Add(new MySqlParameter("@referencia", referencia));
                            datos.Parameters.Add(new MySqlParameter("@formato", formato));
                            datos.ExecuteNonQuery();
                        }
                    }
                }

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }


        [HttpPost]
        public ActionResult GuardarFamilia(FamiliasModels familias)
        {
            var retorno = "0";
            try
            {

                conexion.conectar();

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    if (file.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        string formato = System.IO.Path.GetExtension(file.FileName);

                        using (BinaryReader reader = new BinaryReader(file.InputStream))
                        {
                            byte[] archivo_binario = reader.ReadBytes(file.ContentLength);

                            string sql = "INSERT familias ";
                            sql += "SET codigo = @codigo";
                            sql += ",nombre = @nombre";
                            sql += ",foto = @foto";
                            sql += ",formato = @formato";


                            MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                            datos.Parameters.Add(new MySqlParameter("@foto", archivo_binario));
                            datos.Parameters.Add(new MySqlParameter("@codigo", familias.codigo));
                            datos.Parameters.Add(new MySqlParameter("@nombre", familias.nombre));
                            datos.Parameters.Add(new MySqlParameter("@formato", formato));
                            datos.ExecuteNonQuery();
                        }
                    }
                }    

                retorno = "1";
            }
            catch (Exception error)
            {     
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult EliminarFamilia(FormCollection form_)
        {
            var retorno = 0;
            try
            {
                String codigo = form_["codigo"];
                String referencia = form_["referencia"];
                int verifica = VerificaDatosRelacionFamilia(codigo);
                if (verifica == 1)
                {
                    retorno = 2;
                }
                else if (verifica == -1)
                {
                    retorno = -1;
                }
                else
                {
                    string sql = "DELETE FROM familias WHERE referencia=@referencia";
                    conexion.conectar();
                    MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                    datos.Parameters.Add(new MySqlParameter("@referencia", referencia));
                    datos.ExecuteNonQuery();
                    conexion.cerrar();
                    retorno = 1;

                }
                return Json(retorno);
            }
            catch (Exception error)
            {

                return Json(error.Message);
            }

        }

        public int VerificaDatosRelacionFamilia(String codigo)
        {
            var retorno = 0;
            int existe_familia = 0;
            DataTable tabla_familia = new DataTable();
            try
            {
                String sql_producto = "SELECT familia FROM productos WHERE (familia = " + codigo + ") ";
                conexion.conectar();
                MySqlDataAdapter datos_cta = new MySqlDataAdapter(sql_producto, conexion.con);
                datos_cta.Fill(tabla_familia);
                existe_familia = tabla_familia.Rows.Count;
                if (existe_familia > 0)
                {
                    retorno = 1;
                }
            }
            catch (Exception)
            {
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }
            return retorno;
        }

        [HttpPost]
        public ActionResult ActualizaNombreFamilia(string referencia, string nombre)
        {
            var retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE familias ";
                sql += "SET nombre = @nombre ";
                sql += "WHERE referencia = @referencia ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@nombre", nombre));
                datos.Parameters.Add(new MySqlParameter("@referencia", referencia));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        public ActionResult partialListadoFamilias()
        {
 
            List<FamiliasModels> Listafamilias = new List<FamiliasModels>();

            int retorno = 0;
            string mensaje_error = "";
            string codigo = "";
            string nombre = "";
            string referencia = "";

            try
            {
                String sql;

                sql = "SELECT codigo,nombre,referencia";             
                sql += " FROM familias ";
               
                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                  
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            codigo = d["codigo"].ToString();
                            nombre = d["nombre"].ToString();
                            referencia = d["referencia"].ToString();
                            Listafamilias.Add(new FamiliasModels
                            {
                                codigo = codigo,
                                nombre = nombre,
                                referencia = referencia
                            });

                        }
                    }
                }
                retorno = 1;
                return View(Listafamilias);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

        public ActionResult ActualizarFamilias()
        {
            if (Session["ss_UserCodigo"] == null)
            {
                Session["UserName"] = string.Empty;
                Session.Abandon();
                Session.Clear();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                return RedirectToAction("Index", "Home");
            }
            else
            {             
                return View();
            }
        }

        public JsonResult GetFamilia()
        {
            List<FamiliasModels> familias = new List<FamiliasModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre FROM familias";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    familias.Add(new FamiliasModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(familias, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ComboboxPartialFamilias(string indice, string familia, string producto)
        {
            List<FamiliasModels> familias = new List<FamiliasModels>();


            String sql = "SELECT codigo, nombre FROM familias";
            conexion.conectar();

            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    while (d.Read())
                    {

                        familias.Add(new FamiliasModels
                        {
                            codigo = d["codigo"].ToString(),
                            nombre = d["nombre"].ToString()                           
                        });
                    }
                }
            }
            conexion.cerrar();

            ViewData["indice"] = indice;
            ViewData["familia"] = familia;
            ViewData["codigo_prod"] = producto;

            return PartialView(familias);
        }

    }
}