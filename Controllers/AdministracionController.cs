﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class AdministracionController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        [HttpPost]
        public ActionResult ActualizaPresentacion(string codigo, string presentacion)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET presentacion = @presentacion ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";
                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@presentacion", presentacion));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }


        public JsonResult GetCodPre()
        {
            List<PresentacionModels> codpres = new List<PresentacionModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigopre FROM presentaciones GROUP BY codigopre";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    codpres.Add(new PresentacionModels
                    {
                        codpre = datos_tabla[0].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(codpres, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPresentaciones(string codpre)
        {
            List<PresentacionModels> presentaciones = new List<PresentacionModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre FROM presentaciones WHERE codigopre=" +codpre;
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    presentaciones.Add(new PresentacionModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(presentaciones, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditarDetalleProducto(ProductosModels producto)
        {
            var retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET inddescripcion =@inddescripcion";
                sql += ",titulodescripcion =@titulodescripcion";
                sql += ",descripcion =@descripcion";
                sql += ",titulovideo =@titulovideo";
                sql += ",codigovideo1 =@codigovideo1";
                sql += ",codigovideo2 =@codigovideo2";
                sql += ",ultmodificacion =@ultmodificacion";
                sql += " WHERE codigo =@codigo";


                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@codigo", producto.codigo));
                datos.Parameters.Add(new MySqlParameter("@inddescripcion", producto.inddescripcion));
                datos.Parameters.Add(new MySqlParameter("@titulodescripcion", producto.titulodescripcion));
                datos.Parameters.Add(new MySqlParameter("@descripcion", producto.descripcion));
                datos.Parameters.Add(new MySqlParameter("@titulovideo", producto.titulovideo));
                datos.Parameters.Add(new MySqlParameter("@codigovideo1", producto.codigovideo1));
                datos.Parameters.Add(new MySqlParameter("@codigovideo2", producto.codigovideo2));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                //seo
                //U P D A T E

                //*****************************************************
                // ***** OJO . Hay que pasar el codp del articulo editado****
                //*****************************************************
                sql = "SELECT Productos.codigo as SKU,Productos.nombre as Titulo,IFNULL(Productos.observaciones,'') AS Descripcion,";
                sql += "(SELECT ruta  ";
                sql += "FROM fotos ";
                sql += "WHERE producto=Productos.codigo ORDER BY portada DESC limit 1) AS imagenURL ";
                sql += "FROM Productos where codigo=" + producto.codigo; //<----------- Pasar codp del artículo modificado

                DataTable tbSeo_0 = new DataTable();
                DataTable tbSeo_1 = new DataTable();
                DataTable tbseo_listado = new DataTable();

                conexion.conectar();
                MySqlDataAdapter datWP_0 = new MySqlDataAdapter(sql, conexion.con);
                datWP_0.Fill(tbSeo_0);
                conexion.cerrar();
                conexion.conectarWP();
                string imagen = "";
                foreach (DataRow fila_seo in tbSeo_0.Rows)
                {

                    string titulo = fila_seo["Titulo"].ToString();
                    imagen = fila_seo["imagenURL"].ToString().Replace("~", "https://www.fishboxchile.cl/");
                    string sku = fila_seo["SKU"].ToString();
                    string descripcion = fila_seo["descripcion"].ToString();

                    sql = "update wpposts set ";
                    sql += "post_title=@titulo, ";
                    sql += "post_modified=@post_modified, ";
                    sql += "post_excerpt=@descripcion, post_name=@titulo2 ";
                    sql += " where id=(select post_id from wppostmeta where meta_key='_sku' and meta_value=@sku )";

                    MySqlCommand datos_seo_0 = new MySqlCommand(sql, conexion.conWP);
                    datos_seo_0.Parameters.Add(new MySqlParameter("@titulo", titulo));
                    datos_seo_0.Parameters.Add(new MySqlParameter("@descripcion", descripcion));
                    datos_seo_0.Parameters.Add(new MySqlParameter("@titulo2", titulo.Replace(" ", "-")));
                    datos_seo_0.Parameters.Add(new MySqlParameter("@sku", sku));
                    datos_seo_0.Parameters.Add(new MySqlParameter("@post_modified", DateTime.Now ));
                    datos_seo_0.ExecuteNonQuery();
                }
                conexion.cerrarWP();
                WebClient client = new WebClient();
                Stream stream = client.OpenRead("http://wordpressopencode.cl/wp/sincronizar.php?codigo=" + producto.codigo );




                //F I N   U P D A T E*******



                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }


        [HttpPost]
        public ActionResult ActualizaIndicador(string codigo, string campo, string indicador)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET " + campo + " = @indicador ";
                sql += "WHERE codigo = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@indicador", indicador));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult GuardarProducto(ProductosModels producto)
        {
            var retorno = "0";
            try
            {

                conexion.conectar();
                string sql = "INSERT productos ";
                sql += "SET codigo = @codigo";
                sql += ",nombre = @nombre";
                sql += ",nombrecorto = @nombrecorto";
                sql += ",familia = @familia";
                sql += ",nfamilia = @nfamilia";
                sql += ",unidad = @unidad";
                sql += ",pesogrs = @pesogrs";
                sql += ",precio = @precio";
                sql += ",agotado = @agotado";
                sql += ",preciooferta = @preciooferta";
                sql += ",venceoferta = @venceoferta";
                sql += ",oferta = @oferta";
                sql += ",indpagweb = @indpagweb";
                sql += ",observaciones = @observaciones";
                sql += ",presentacion =@presentacion";
                sql += ",indcabezaesquelon =@indcabezaesquelon";
                sql += ",inddescripcion =@inddescripcion";
                sql += ",titulodescripcion =@titulodescripcion";
                sql += ",descripcion =@descripcion";
                sql += ",titulovideo =@titulovideo";
                sql += ",codigovideo1 =@codigovideo1";
                sql += ",codigovideo2 =@codigovideo2";
                sql += ",cantidaddesde =@cantidaddesde";
                sql += ",ultmodificacion =@ultmodificacion";


                string venceoferta_format = null;
                if (producto.oferta == 1)
                {
                    venceoferta_format = DateTime.Parse(producto.venceoferta.Replace("T", " ")).ToString("yyyy-MM-dd HH:mm");
                }
                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@codigo", producto.codigo));
                datos.Parameters.Add(new MySqlParameter("@nombre", producto.nombre));
                datos.Parameters.Add(new MySqlParameter("@nombrecorto", producto.nombrecorto));
                datos.Parameters.Add(new MySqlParameter("@familia", producto.familia));
                datos.Parameters.Add(new MySqlParameter("@nfamilia", producto.nfamilia));
                datos.Parameters.Add(new MySqlParameter("@unidad", producto.unidad));
                datos.Parameters.Add(new MySqlParameter("@pesogrs", producto.pesogrs));
                datos.Parameters.Add(new MySqlParameter("@precio", producto.precio));
                datos.Parameters.Add(new MySqlParameter("@agotado", producto.agotado));
                datos.Parameters.Add(new MySqlParameter("@preciooferta", producto.preciooferta));
                datos.Parameters.Add(new MySqlParameter("@venceoferta", venceoferta_format));
                datos.Parameters.Add(new MySqlParameter("@oferta", producto.oferta));
                datos.Parameters.Add(new MySqlParameter("@indpagweb", producto.indpagweb));
                datos.Parameters.Add(new MySqlParameter("@observaciones", producto.observaciones));
                datos.Parameters.Add(new MySqlParameter("@presentacion", producto.indpresentacion));
                datos.Parameters.Add(new MySqlParameter("@indcabezaesquelon", producto.indcabezaesquelon));
                datos.Parameters.Add(new MySqlParameter("@inddescripcion", producto.inddescripcion));
                datos.Parameters.Add(new MySqlParameter("@titulodescripcion", producto.titulodescripcion));
                datos.Parameters.Add(new MySqlParameter("@descripcion", producto.descripcion));
                datos.Parameters.Add(new MySqlParameter("@titulovideo", producto.titulovideo));
                datos.Parameters.Add(new MySqlParameter("@codigovideo1", producto.codigovideo1));
                datos.Parameters.Add(new MySqlParameter("@codigovideo2", producto.codigovideo2));
                datos.Parameters.Add(new MySqlParameter("@cantidaddesde", producto.cantidaddesde));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }


        [HttpPost]
        public ActionResult ActualizaOferta(string codigo, string preciooferta, string venceoferta, string oferta)
        {
            var retorno = "0";
            try
            {

                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET preciooferta = @preciooferta";
                sql += ",preciooferta = @preciooferta";
                sql += ",venceoferta = @venceoferta";
                sql += ",oferta = @oferta ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";

                string venceoferta_format = DateTime.Parse(venceoferta.Replace("T", " ")).ToString("yyyy-MM-dd HH:mm");

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@preciooferta", preciooferta));
                datos.Parameters.Add(new MySqlParameter("@venceoferta", venceoferta_format));
                datos.Parameters.Add(new MySqlParameter("@oferta", oferta));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult ActualizaFamiliaProducto(string codigo, string familia, string nfamilia)
        {
            string retorno = "0";
            try
            {

                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET familia = @familia";
                sql += ",nfamilia = @nfamilia ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@familia", familia));
                datos.Parameters.Add(new MySqlParameter("@nfamilia", nfamilia));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult ActualizaIndStock(string codigo, string indicador)
        {
            string retorno = "0";
            try
            {
                List<string> coreosLista = new List<string>();
                //String ts = "start transaction";
                //MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                //datos_ts.ExecuteScalar();


                String sql;

                sql = "UPDATE productos ";
                sql += "SET agotado = @agotado ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";
                
                conexion.conectar();

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@agotado", indicador));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

            



                //String commit = "commit";
                //MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                //datos_commit.ExecuteScalar();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();

                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult ActualizaIndPagWebProducto(string codigo, string indicador)
        {
            string retorno = "0";
            try
            {

                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET indpagweb = @indpagweb ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@indpagweb", indicador));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult ActualizaCampoProducto(string codigo, string campo, string valorcampo)
        {
            var retorno = "0";
            try
            {

                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET " + campo + " = @campo ";
                sql += ",ultmodificacion =@ultmodificacion ";
                sql += "WHERE codigo = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@campo", valorcampo));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult ActualizaObservacionProducto(FormCollection form_)
        {
            string retorno = "0";
            try
            {
                string codigo = form_["codigo"];
                string obs = form_["obs"];
                conexion.conectar();
                string sql = "UPDATE productos ";
                sql += "SET observaciones = @observaciones ";
                sql += ",ultmodificacion = @ultmodificacion ";
                sql += "WHERE codigo = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@observaciones", obs));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.Parameters.Add(new MySqlParameter("@ultmodificacion", DateTime.Now));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                var erro = error.Message;
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }




        public ActionResult VerProductos()
        {
            if (Session["ss_UserCodigo"] == null)
            {
                Session["UserName"] = string.Empty;
                Session.Abandon();
                Session.Clear();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                return RedirectToAction("Index", "Home");
            }
            else
            {


                DateTime fecha_actual = DateTime.Now;
                DateTime fecha_actual_masundia = fecha_actual.AddDays(1);
                string fecha_minima = fecha_actual_masundia.ToString("yyyy-MM-dd HH:mm").Replace(" ", "T");
                //datetime fecha = Convert.ToDateTime(textbox1.text, new CultureInfo("es-ES"));
                ViewData["fecha_minima"] = fecha_minima;
                return View();
            }
        }

        public ActionResult partialListadoProductos(string familia)
        {
            DateTime fecha_actual = DateTime.Now;
            DateTime fecha_actual_masundia = fecha_actual.AddDays(1);
            string fecha_minima = fecha_actual_masundia.ToString("yyyy-MM-dd HH:mm").Replace(" ", "T");
            //datetime fecha = Convert.ToDateTime(textbox1.text, new CultureInfo("es-ES"));
            ViewData["fecha_minima"] = fecha_minima;

            List<ProductosModels> ListaProductos = new List<ProductosModels>();

            int retorno = 0;
            string mensaje_error = "";
            string codigo = "";
            string nombre = "";
            string unidad = "";
            string observaciones = "";
            string pesogrs = "";
            int precio = 0;
            int agotado = 0;
            int idfamilia = 0;
            string venceoferta = "";
            int oferta = 0;
            double preciooferta = 0;
            int indpagweb = 0;
            int indpresentacion = 0;
            int indcabezaesquelon = 0;
            int inddescripcion = 0;
            string titulodescripcion = "";
            string descripcion = "";
            string titulovideo = "";
            string codigovideo1 = "";
            string codigovideo2 = "";
            string nombrecorto = "";
            double cantidaddesde = 0;

            try
            {
                String sql;

                sql = "SELECT productos.codigo,productos.nombre,productos.unidad,IFNULL(productos.observaciones,'') AS observaciones,productos.pesogrs,";
                sql += "productos.precio,";
                sql += "productos.agotado,";
                sql += "productos.preciooferta,";
                sql += "DATE_FORMAT(productos.venceoferta,'%Y-%m-%dT%H:%i') AS venceoferta,";
                sql += "productos.familia,";
                sql += "productos.indpagweb,";
                sql += "CAST(productos.oferta AS SIGNED) AS oferta,";
                sql += "productos.nombrecorto,";
                sql += "CAST(productos.indcabezaesquelon AS SIGNED) AS indcabezaesquelon,";
                sql += "CAST(productos.presentacion AS SIGNED) AS presentacion,";
                sql += "CAST(productos.inddescripcion AS SIGNED) AS inddescripcion,";
                sql += "productos.titulodescripcion,";
                sql += "productos.descripcion,";
                sql += "productos.titulovideo,";
                sql += "productos.codigovideo1,";
                sql += "productos.codigovideo2,";
                sql += "productos.cantidaddesde ";
                sql += "FROM productos ";
                sql += "WHERE productos.nuevooc=0 ";

                if (familia != "")
                {
                    sql += "AND productos.familia=@familia";
                }

                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    if (familia != "")
                    {
                        cmd.Parameters.AddWithValue("@familia", familia);
                    }
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            codigo = d["codigo"].ToString();
                            nombre = d["nombre"].ToString();
                            unidad = d["unidad"].ToString();
                            observaciones = d["observaciones"].ToString();
                            pesogrs = d["pesogrs"].ToString();
                            precio = Convert.ToInt32(d["precio"].ToString());
                            agotado = Convert.ToInt32(d["agotado"].ToString());
                            venceoferta = d["venceoferta"].ToString();
                            oferta = Convert.ToInt32(d["oferta"].ToString());
                            preciooferta = Convert.ToDouble(d["preciooferta"]);
                            indpagweb = Convert.ToInt32(d["indpagweb"].ToString());
                            nombrecorto = d["nombrecorto"].ToString();
                            idfamilia = Convert.ToInt32(d["familia"].ToString());
                            indpresentacion = Convert.ToInt32(d["presentacion"].ToString());
                            indcabezaesquelon = Convert.ToInt32(d["indcabezaesquelon"].ToString());
                            inddescripcion = Convert.ToInt32(d["inddescripcion"].ToString());
                            titulodescripcion = d["titulodescripcion"].ToString();
                            descripcion = d["descripcion"].ToString();
                            titulovideo = d["titulovideo"].ToString();
                            codigovideo1 = d["codigovideo1"].ToString();
                            codigovideo2 = d["codigovideo2"].ToString();
                            cantidaddesde = Convert.ToDouble(d["cantidaddesde"]);

                            ListaProductos.Add(new ProductosModels
                            {
                                codigo = codigo,
                                nombre = nombre,
                                unidad = unidad,
                                observaciones = observaciones,
                                pesogrs = pesogrs,
                                precio = precio,
                                agotado = agotado,
                                venceoferta = venceoferta,
                                oferta = oferta,
                                preciooferta = preciooferta,
                                indpagweb = indpagweb,
                                nombrecorto = nombrecorto,
                                familia = idfamilia,
                                presentacion = indpresentacion,
                                indcabezaesquelon = indcabezaesquelon,
                                inddescripcion = inddescripcion,
                                titulodescripcion = titulodescripcion,
                                titulovideo = titulovideo,
                                descripcion = descripcion,
                                codigovideo1 = codigovideo1,
                                codigovideo2 = codigovideo2,
                                cantidaddesde=cantidaddesde

                            });

                        }
                    }
                }
                retorno = 1;
                return View(ListaProductos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

    }
}