﻿using WebFBX.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebFBX.Controllers
{
    public class ValidacionbdController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

     
        public ActionResult nombre_existe_producto(FormCollection form_)
        {
            String nombre = form_["nombre"];
            bool ReturnValue = false;
            int retornoJson = 2;
            ReturnValue = VerificaExistenciaNombreProducto(nombre);
            if (ReturnValue == true)
            {
                retornoJson = 2;
            }
            else
            {
                retornoJson = 1;
            }
            return Json(retornoJson);
        }
        public Boolean VerificaExistenciaNombreProducto(String nombre)
        {
            var retorno = false;
            int existe_query = 0;
            DataTable tabla = new DataTable();
            try
            {
                String sql = "SELECT nombre FROM productos  WHERE (nombre= '" + nombre + "') ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(tabla);
                existe_query = tabla.Rows.Count;
                if (existe_query > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }
            return retorno;

        }

        public ActionResult codigo_existe_descuento(FormCollection form_)
        {
            String codigo = form_["codigo"];
            bool ReturnValue = false;
            int retornoJson = 2;
            ReturnValue = VerificaExistenciaCodigoDescuento(codigo);
            if (ReturnValue == true)
            {
                retornoJson = 2;
            }
            else
            {
                retornoJson = 1;
            }
            return Json(retornoJson);
        }
        public ActionResult codigo_existe_familia(FormCollection form_)
        {
            String codigo = form_["codigo"];
            bool ReturnValue = false;
            int retornoJson = 2;
            ReturnValue = VerificaExistenciaCodigoFamilia(codigo);
            if (ReturnValue == true)
            {
                retornoJson = 2;
            }
            else
            {
                retornoJson = 1;
            }
            return Json(retornoJson);
        }
        public ActionResult codigo_existe_producto(FormCollection form_)
        {
            String codigo = form_["codigo"];
            bool ReturnValue = false;
            int retornoJson = 2;
            ReturnValue = VerificaExistenciaCodigoProducto(codigo);
            if (ReturnValue == true)
            {
                retornoJson = 2;
            }
            else
            {
                retornoJson = 1;
            }
            return Json(retornoJson);
        }
        public Boolean VerificaExistenciaCodigoDescuento(String codigo)
        {
            var retorno = false;
            int existe_query = 0;
            DataTable tabla = new DataTable();
            try
            {
                String sql = "SELECT coddesc FROM descuentos  WHERE (coddesc= '" + codigo + "') ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(tabla);
                existe_query = tabla.Rows.Count;
                if (existe_query > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }
            return retorno;

        }
        public Boolean VerificaExistenciaCodigoFamilia(String codigo)
        {
            var retorno = false;
            int existe_query = 0;
            DataTable tabla = new DataTable();
            try
            {
                String sql = "SELECT codigo FROM familias  WHERE (codigo= '" + codigo + "') ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(tabla);
                existe_query = tabla.Rows.Count;
                if (existe_query > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }
            return retorno;

        }
        public Boolean VerificaExistenciaCodigoProducto(String codigo)
        {
            var retorno = false;
            int existe_query = 0;
            DataTable tabla = new DataTable();
            try
            {
                String sql = "SELECT codigo FROM productos  WHERE (codigo= '" + codigo + "') ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(tabla);
                existe_query = tabla.Rows.Count;
                if (existe_query > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }
            return retorno;

        }


        public static string FormatearRUT(string rut)
        {

            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    //if (cont == 3 && i != 0)
                    //{
                    //    format = "." + format;
                    //    cont = 0;
                    //}
                }
                return format;
            }

        }


    }
}
