﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class UbicacionController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();
        public JsonResult GetComuna()
        {
            List<ComunaModels> orden = new List<ComunaModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre FROM comunas WHERE indbloqueo=0";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    orden.Add(new ComunaModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(orden, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCondominio(string comuna)
        {
            List<CondominioModels> condominios = new List<CondominioModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre FROM condominios WHERE comuna='"+comuna+"'";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    condominios.Add(new CondominioModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(condominios, JsonRequestBehavior.AllowGet);
        }
    }
}