﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class ListaEsperaController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        [HttpPost]
        public ActionResult EnviarEmailUsuario(string codigo, string nombrecliente, string correocliente, string telefono,string nombreproducto)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                //String ts = "start transaction";
                //MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                //datos_ts.ExecuteScalar();


                String sql;

                sql = "UPDATE productos ";
                sql += "SET agotado = @agotado ";
                sql += "WHERE codigo = @codigo ";


                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@agotado", "0"));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.ExecuteNonQuery();


                string emailenvio = Session["ss_email"].ToString();
                string passwordenvio = Session["ss_password"].ToString();
                string host = Session["ss_smtp"].ToString();

                int puerto = Convert.ToInt32(Session["ss_puerto"]);

                SmtpClient client = new SmtpClient
                {
                    Host = host,
                    Port = puerto,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailenvio, passwordenvio)
                };
                string titulo_subjet = "";

                string body = "";

                titulo_subjet = "Aviso de producto disponible";
                body = "<p>Estimado(a) " + nombrecliente + "</p>";
                body = body + "<p>Le informamos que el producto " + nombreproducto + " ya se encuentra disponible</p>";
                body = body + "Atte.<br/>FISH BOX<br/>";

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(emailenvio),
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = true,
                    Priority = MailPriority.Normal,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    Subject = titulo_subjet,
                    Body = body,
                };

                mail.To.Add(new MailAddress(correocliente));

                //foreach (var c in copia)
                //{
                //    mail.CC.Add(new MailAddress(c));
                //}

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object s
                        , X509Certificate certificate
                        , X509Chain chain
                        , SslPolicyErrors sslPolicyErrors)
                    { return true; };

                client.Send(mail);

                sql = "UPDATE listaespera ";
                sql += "SET enviado = @enviado ";
                sql += "WHERE telefono = @telefono ";

                MySqlCommand datos_esp = new MySqlCommand(sql, conexion.con);
                datos_esp.Parameters.AddWithValue("@telefono", telefono);
                datos_esp.Parameters.AddWithValue("@enviado", 1);
                datos_esp.ExecuteNonQuery();


                //String commit = "commit";
                //MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                //datos_commit.ExecuteScalar();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();

                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        public ActionResult partialListadoClientesEspera(string codigopro, string nombreproducto, int agotado)
        {

            List<ClientesEsperaModels> ListaClientes = new List<ClientesEsperaModels>();

            int retorno = 0;
            string mensaje_error = "";
            string fecha = "";
            string telefono = "";
            string correo = "";
            string clientes = "";
            try
            {
                String sql;
                ViewData["codigopro"] = codigopro;
                ViewData["nombreproducto"] = nombreproducto;
                if (agotado == 1)
                {
                    ViewData["agotado"] = "Agotado: Sí";
                }
                else
                {
                    ViewData["agotado"] = "Agotado: No";
                }

                int cant_usuarios = 0;

                sql = " SELECT listado.nombrecliente,listado.telefono, DATE_FORMAT(listado.fecha,'%d/%m/%Y') AS fecha,listado.correo FROM";
                sql += " (SELECT l.nombrecliente,l.telefono,l.fecha,correo FROM listaespera l WHERE l.enviado=0 AND l.producto=@producto ORDER BY fecha DESC) listado GROUP BY listado.telefono";
                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    cmd.Parameters.AddWithValue("@producto", codigopro);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            cant_usuarios++;
                            clientes = d["nombrecliente"].ToString();
                            telefono = d["telefono"].ToString();
                            fecha = d["fecha"].ToString();
                            correo = d["correo"].ToString();


                            ListaClientes.Add(new ClientesEsperaModels
                            {
                                nombre = clientes,
                                telefono = telefono,
                                fecha = fecha,
                                email = correo,
                                codigopro = codigopro,
                                nombreproducto = nombreproducto
                            });

                        }
                    }
                }
                ViewData["cant_usuarios"] = "N° Usuarios:" + cant_usuarios.ToString();

                retorno = 1;
                return View(ListaClientes);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

        [HttpPost]
        public ActionResult EnviarEmailUsuariosEnEspera(string codigo, string indicador)
        {
            string retorno = "0";
            try
            {
                List<string> coreosLista = new List<string>();
                conexion.conectar();
                int existe_espera = 0;
                //String ts = "start transaction";
                //MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                //datos_ts.ExecuteScalar();


                String sql;

                sql = "UPDATE productos ";
                sql += "SET agotado = @agotado ";
                sql += "WHERE codigo = @codigo ";


                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@agotado", "0"));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.ExecuteNonQuery();

                //if (indicador == "0")
                //{



                sql = "SELECT correo,nombreproducto ";
                sql += "FROM listaespera WHERE producto=@producto AND enviado=@enviado GROUP BY telefono";

                string correo = "";
                string nombreproducto = "";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    cmd.Parameters.AddWithValue("@producto", codigo);
                    cmd.Parameters.AddWithValue("@enviado", "0");

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            existe_espera = 1;
                            correo = d["correo"].ToString();
                            nombreproducto = d["nombreproducto"].ToString();

                            coreosLista.Add(correo);

                        }
                    }
                }

                if (existe_espera == 1)
                {
                    string[] correosArray = coreosLista.ToArray();
                    string correosJoin = string.Join(";", correosArray);
                    string emailenvio = Session["ss_email"].ToString();
                    string passwordenvio = Session["ss_password"].ToString();
                    string host = Session["ss_smtp"].ToString();

                    int puerto = Convert.ToInt32(Session["ss_puerto"]);

                    SmtpClient client = new SmtpClient
                    {
                        Host = host,
                        Port = puerto,
                        EnableSsl = true,
                        Timeout = 10000,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(emailenvio, passwordenvio)
                    };
                    string titulo_subjet = "";

                    string body = "";

                    titulo_subjet = "Aviso de producto disponible";
                    body = "<p>Estimado(a) " + "cliente" + "</p>";
                    body = body + "<p>Le informamos que el producto " + nombreproducto + " ya se encuentra disponible</p>";
                    body = body + "Atte.<br/>FISH BOX<br/>";

                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(emailenvio),
                        BodyEncoding = Encoding.UTF8,
                        IsBodyHtml = true,
                        Priority = MailPriority.Normal,
                        DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                        Subject = titulo_subjet,
                        Body = body,
                    };
                    string[] csv = correosJoin.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var s in csv)
                    {
                        mail.To.Add(new MailAddress(s));
                    }
                    //foreach (var c in copia)
                    //{
                    //    mail.CC.Add(new MailAddress(c));
                    //}

                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate (object s
                            , X509Certificate certificate
                            , X509Chain chain
                            , SslPolicyErrors sslPolicyErrors)
                        { return true; };

                    client.Send(mail);

                    sql = "UPDATE listaespera ";
                    sql += "SET enviado = @enviado ";
                    sql += "WHERE producto = @producto ";

                    MySqlCommand datos_esp = new MySqlCommand(sql, conexion.con);
                    datos_esp.Parameters.AddWithValue("@producto", codigo);
                    datos_esp.Parameters.AddWithValue("@enviado", 1);
                    datos_esp.ExecuteNonQuery();
                }
                //}




                //String commit = "commit";
                //MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                //datos_commit.ExecuteScalar();

                retorno = "1";
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();

                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        public ActionResult GetFoto(int codigo)
        {
            try
            {
                conexion.conectar();
                string sql = "SELECT foto,formato AS extension FROM fotos WHERE producto=" + codigo + "  ORDER BY portada DESC limit 1";
                byte[] imagen = null;
                string extension = "";
                string nombre = "foto";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            imagen = (byte[])d["foto"];
                            extension = (string)d["extension"];
                            nombre = nombre + codigo.ToString() + "." + extension;
                        }
                        else
                        {
                            var folder = Server.MapPath("~/img/imagen_sin_foto.png");
                            string contentType = "image/png";
                            byte[] foto_blanco = System.IO.File.ReadAllBytes(folder);
                            //MySqlConnection.ClearAllPools();
                            return File(foto_blanco, contentType, "imagen.png");
                        }
                    }
                }
                conexion.cerrar();
                //return File(imagen, "image/jpeg");
                return File(imagen, "application/octet-stream", nombre);

            }
            catch (Exception ex)
            {
                string erro = ex.Message;
                return Json(ex);
            }
        }
        public ActionResult VerListaEspera()
        {
            if (Session["ss_UserCodigo"] == null)
            {
                Session["UserName"] = string.Empty;
                Session.Abandon();
                Session.Clear();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }



        public ActionResult partialListadoProductoEspera()
        {

            List<ProductosEsperaModels> ListaProductos = new List<ProductosEsperaModels>();

            int retorno = 0;
            string mensaje_error = "";
            string codigo = "";
            string nombre = "";
            string clientes = "";
            int agotado = 0;
            try
            {
                String sql;

                sql = " SELECT l.producto,l.nombreproducto,p.agotado,(SELECT COUNT(*)";
                sql += " FROM (SELECT producto FROM listaespera  GROUP BY telefono) AS usua WHERE usua.producto=l.producto) AS cliente";
                sql += " FROM listaespera l LEFT JOIN productos p ON l.producto=p.codigo WHERE l.enviado=0 GROUP BY l.producto";

                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            codigo = d["producto"].ToString();
                            nombre = d["nombreproducto"].ToString();
                            agotado = Convert.ToInt32(d["agotado"].ToString());
                            clientes = d["cliente"].ToString();
                            if (clientes == "" ||clientes==null)
                            {
                                clientes = "0";
                            }

                            ListaProductos.Add(new ProductosEsperaModels
                            {
                                codigo = codigo,
                                nombre = nombre,
                                agotado = agotado,
                                clientes=Convert.ToInt32(clientes)
                            });

                        }
                    }
                }
                retorno = 1;
                return View(ListaProductos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }
    }
}