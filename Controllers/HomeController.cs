﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;

namespace WebFBX.Controllers
{
    public class HomeController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        public ActionResult Index(string detalle)
        {
            if (detalle != "1")
            {
                Session["ss_familia"] = null;
            }
            ViewData["detalleurl"] = detalle;
            ViewBag.Title = "Productos Frescos";
            conexion.conectar();
            string tramo1 = "0";
            string tramo2 = "0";
            string valort1 = "0";
            string valort2 = "0";
            string compraminima = "0";
            string topedescuento = "0";
            string sql = "SELECT DATE_FORMAT(venceoferta,'%Y-%m-%d %H:%i:%s') AS venceoferta FROM productos WHERE oferta=1 AND productos.venceoferta >= NOW() ORDER BY venceoferta DESC LIMIT 1";
            string fechaoferta = "";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        fechaoferta = d["venceoferta"].ToString();
                    }
                    else
                    {
                       
                    }
                }
            }
            sql = "SELECT tramo1,tramo2,topedescuento,valort1,valort2,compraminima FROM parametros LIMIT 1";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        tramo1 = d["tramo1"].ToString();
                        tramo2 = d["tramo2"].ToString();
                        valort1 = d["valort1"].ToString();
                        valort2 = d["valort2"].ToString();
                        compraminima = d["compraminima"].ToString();
                        topedescuento = d["topedescuento"].ToString();
                    }
                    else
                    {

                    }
                }
            }
            string codigo = "WWWFISHBOX";
            string vendedor = "99";
            sql = "SELECT vendedor FROM usuarios WHERE codigo=@codigo";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        vendedor = d["vendedor"].ToString();                      
                    }
                    else
                    {

                    }
                }
            }
            conexion.cerrar();

            ViewData["venceoferta_vd"] = fechaoferta;
            ViewData["tramo1_vd"] = tramo1;
            ViewData["tramo2_vd"] = tramo2;
            ViewData["valort1_vd"] = valort1;
            ViewData["valort2_vd"] = valort2;
            ViewData["vendedor_vd"] = vendedor;
            Session["valorCompraMinima"] = compraminima;
            ViewData["topedescuento_vd"] = topedescuento;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}