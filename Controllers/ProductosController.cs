﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class ProductosController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();
        ///*# sourceMappingURL=style.css.map */
        ///

        [HttpPost]
        public ActionResult CreaListaEspera(string telefono, string correo, string producto, string nombre, string nombrecliente)
        {
            int retorno = 0;
            string errormsj = "";
            try
            {
                string fechaactual = DateTime.Now.ToString("yyyy-MM-dd");
                conexion.conectar();
                string sql = "SELECT producto FROM listaespera WHERE telefono=@telefono AND producto=@producto AND fecha=@fechaactual limit 1";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    cmd.Parameters.AddWithValue("@telefono", telefono);
                    cmd.Parameters.AddWithValue("@producto", producto);
                    cmd.Parameters.AddWithValue("@fechaactual", fechaactual);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            retorno = 2;
                        }
                        else
                        {

                        }
                    }
                }

                if (retorno == 0)
                {
                    sql = "INSERT listaespera ";
                    sql += "SET telefono = @telefono";
                    sql += ",correo = @correo ";
                    sql += ",producto = @producto ";
                    sql += ",nombreproducto = @nombreproducto ";
                    sql += ",nombrecliente = @nombrecliente ";
                    sql += ",fecha = @fecha ";
                    MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                    datos.Parameters.Add(new MySqlParameter("@producto", producto));
                    datos.Parameters.Add(new MySqlParameter("@telefono", telefono));
                    datos.Parameters.Add(new MySqlParameter("@correo", correo));
                    datos.Parameters.Add(new MySqlParameter("@nombreproducto", nombre));
                    datos.Parameters.Add(new MySqlParameter("@nombrecliente", nombrecliente));
                    datos.Parameters.Add(new MySqlParameter("@fecha", fechaactual));
                    datos.ExecuteNonQuery();
                    retorno = 1;

                }
            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                errormsj = error.Message;
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(new { retornoJson = retorno, errorJson = errormsj }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult partialFoto(string codigo)
        {
            ViewData["codigo"] = codigo;
            return View();
        }
        [HttpPost]
        public ActionResult partialFotoModificar(string codigo)
        {
            ViewData["codigo"] = codigo;
            return View();
        }
        public JsonResult VaciarCarrito()
        {
            Session["carrito"] = null;
            Session["contador"] = 0;
            Session["codigo_descuento"] = null;
            return Json(1);
        }

        public JsonResult GuardaCarrito(List<ProductosModels> carrito, double contador)
        {
            Session["carrito"] = carrito;
            Session["contador"] = contador;
            return Json(1);
        }
        [HttpPost]
        public ActionResult obtieneCarritoGuardado()
        {
            var p = Session["contador"];
            List<ProductosModels> listadoCarritoGuardado = Session["carrito"] as List<ProductosModels>;
            return Json(new { listadoCarritoGuardado = listadoCarritoGuardado.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFoto(int codigo)
        {
            try
            {
                conexion.conectar();
                string sql = "SELECT foto,formato AS extension FROM fotos WHERE producto=" + codigo + "  ORDER BY portada DESC limit 1";
                byte[] imagen = null;
                string extension = "";
                string nombre = "foto";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            imagen = (byte[])d["foto"];
                            extension = (string)d["extension"];
                            nombre = nombre + codigo.ToString() + "." + extension;
                        }
                        else
                        {
                            var folder = Server.MapPath("~/img/imagen_sin_foto.png");
                            string contentType = "image/png";
                            byte[] foto_blanco = System.IO.File.ReadAllBytes(folder);
                            //MySqlConnection.ClearAllPools();
                            return File(foto_blanco, contentType, "imagen.png");
                        }
                    }
                }
                conexion.cerrar();
                //return File(imagen, "image/jpeg");
                return File(imagen, "application/octet-stream", nombre);

            }
            catch (Exception ex)
            {
                string erro = ex.Message;
                return Json(ex);
            }
        }

        //public ActionResult NormaSeo()
        //{
        //    return View();
        //}

        public ActionResult DetalleProducto(string codp)
        {

            //            **************************************
            String sql;
            try
            {
                int idCodp = Int32.Parse(codp);
                String sqlWP;
                DataTable tablaSeo = new DataTable();
                sqlWP = "call ProductoSEO (" + codp + ", '', '')";
                conexion.conectarWP();
                MySqlDataAdapter datosWP = new MySqlDataAdapter(sqlWP, conexion.conWP);
                datosWP.Fill(tablaSeo);
                conexion.cerrarWP();
                if (tablaSeo.Rows.Count > 0)
                {
                    Response.Redirect(tablaSeo.Rows[0]["post_name"].ToString());
                    codp = null;
                }
                else
                {
                    sql = "SELECT Productos.codigo as SKU,Productos.nombre as Titulo,Productos.unidad,IFNULL(Productos.observaciones,'') AS Descripcion,Productos.pesogrs,";
                    sql += "Productos.precio,";
                    sql += "Productos.agotado,";
                    sql += "Productos.preciooferta,Productos.venceoferta,";
                    sql += "CASE ";
                    sql += "WHEN Productos.oferta=1 AND Productos.venceoferta >= NOW() THEN 1 ";
                    sql += "ELSE 0 ";
                    sql += "END ";
                    sql += "AS ofertavigente,";
                    sql += "(SELECT ruta  ";
                    sql += "FROM fotos ";
                    sql += "WHERE producto=Productos.codigo ORDER BY portada DESC limit 1) AS imagenURL,";
                    sql += "CAST(Productos.indcabezaesquelon AS SIGNED) AS indcabezaesquelon,";
                    sql += "CAST(Productos.presentacion AS SIGNED) AS presentacion,";
                    sql += "CAST(Productos.inddescripcion AS SIGNED) AS inddescripcion,";
                    sql += "Productos.costo,";
                    sql += "CASE ";
                    sql += "WHEN Productos.cantidaddesde=0 THEN 1 ";
                    sql += "ELSE Productos.cantidaddesde ";
                    sql += "END ";
                    sql += "AS cantidaddesde ";
                    sql += "FROM Productos ";
                    sql += "WHERE codigo=" +codp;

                    DataTable tablaSeo_0 = new DataTable();
                    DataTable tablaSeo_1 = new DataTable();
                    DataTable tablaSeo_listado = new DataTable();

                    conexion.conectar();
                    MySqlDataAdapter datosWP_0 = new MySqlDataAdapter(sql, conexion.con);
                    datosWP_0.Fill(tablaSeo_0);
                    conexion.cerrar();

                    foreach (DataRow fila_seo in tablaSeo_0.Rows)
                    {
                        string titulo = fila_seo["Titulo"].ToString();
                        string sku = fila_seo["SKU"].ToString();
                        string descripcion = fila_seo["descripcion"].ToString();
                        string imagen = fila_seo["imagenURL"].ToString().Replace("~", "https://www.fishboxchile.cl/");

                        sql = "SELECT  * FROM `wpwc_product_meta_lookup` WHERE sku='" + sku + "'";
                        conexion.conectarWP();
                        MySqlDataAdapter datosWP_listado = new MySqlDataAdapter(sql, conexion.conWP);
                        datosWP_listado.Fill(tablaSeo_listado);
                        if (tablaSeo_listado.Rows.Count == 0)
                        {
                            sql = "insert into wpposts(ping_status,";
                            sql += "post_author, post_title, post_type, ";
                            sql += "post_content, post_status, post_name) ";
                            sql += " values('closed', 1, @titulo, @product, @descripcion, @publish, @titulo2)";


                            MySqlCommand datos_seo_0 = new MySqlCommand(sql, conexion.conWP);
                            datos_seo_0.Parameters.Add(new MySqlParameter("@titulo", titulo));
                            datos_seo_0.Parameters.Add(new MySqlParameter("@product", "product"));
                            datos_seo_0.Parameters.Add(new MySqlParameter("@descripcion", descripcion));
                            datos_seo_0.Parameters.Add(new MySqlParameter("@publish", "publish"));
                            datos_seo_0.Parameters.Add(new MySqlParameter("@titulo2", titulo.Replace(" ", "-")));
                            datos_seo_0.ExecuteNonQuery();
                            sql = "SELECT id from wpposts order by id desc limit 1";

                            MySqlDataAdapter datosWP_1 = new MySqlDataAdapter(sql, conexion.conWP);
                            datosWP_1.Fill(tablaSeo_1);
                            foreach (DataRow fila_seo_2 in tablaSeo_1.Rows)
                            {
                                string id = fila_seo_2["id"].ToString();
                                sql = "insert into wpwc_product_meta_lookup(product_id, sku) values(@id,@sku)";
                                MySqlCommand datos_seo_1 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_1.Parameters.Add(new MySqlParameter("@id", id));
                                datos_seo_1.Parameters.Add(new MySqlParameter("@sku", sku));
                                datos_seo_1.ExecuteNonQuery();

                                sql = "insert into wppostmeta (post_id,meta_key,meta_value) values(@id,@sku2,@sku)";
                                MySqlCommand datos_seo_2 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_2.Parameters.Add(new MySqlParameter("@id", id));
                                datos_seo_2.Parameters.Add(new MySqlParameter("@sku2", "_sku"));
                                datos_seo_2.Parameters.Add(new MySqlParameter("@sku", sku));
                                datos_seo_2.ExecuteNonQuery();
                                string id2 = (Convert.ToInt32(fila_seo_2["id"]) + 1).ToString();

                                sql = "insert into wppostmeta(post_id, meta_key, meta_value)";
                                sql += " values(@id, '_thumbnail_id', @id2)";

                                MySqlCommand datos_seo_3 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_3.Parameters.Add(new MySqlParameter("@id", id));
                                datos_seo_3.Parameters.Add(new MySqlParameter("@id2", id2));
                                datos_seo_3.ExecuteNonQuery();


                                sql = "insert into wppostmeta (post_id,meta_key,meta_value) values(@id2,@wp_attached_file,@img)";
                                MySqlCommand datos_seo_4 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_4.Parameters.Add(new MySqlParameter("@id2", id2));
                                datos_seo_4.Parameters.Add(new MySqlParameter("@wp_attached_file", "_wp_attached_file"));
                                datos_seo_4.Parameters.Add(new MySqlParameter("@img", "2021/03/Imagen" + id + ".png"));
                                datos_seo_4.ExecuteNonQuery();

                                

                                 sql = "insert into wpposts (post_author,post_title,post_type,post_name,post_parent,guid,post_mime_type,post_status,ping_status)";
                                sql += " values(1,@imatit,'attachment',@imatit,@id,@img2,'image/png','inherit','closed')";
                                MySqlCommand datos_seo_5 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_5.Parameters.Add(new MySqlParameter("@imatit", "Imagen" + id));
                                datos_seo_5.Parameters.Add(new MySqlParameter("@id", id));
                                datos_seo_5.Parameters.Add(new MySqlParameter("@img2", "http://wordpressopencode.cl/wp/wp-content/uploads/2021/03/Imagen" + id + ".png"));
                                datos_seo_5.ExecuteNonQuery();

                                sql = "insert into wppostmeta(post_id, meta_key, meta_value) values(@post_id,@meta_key,@meta_value)";
                                MySqlCommand datos_seo_6 = new MySqlCommand(sql, conexion.conWP);
                                datos_seo_6.Parameters.Add(new MySqlParameter("@post_id", id2));
                                datos_seo_6.Parameters.Add(new MySqlParameter("@meta_key", "_wp_attachment_metadata"));
                                datos_seo_6.Parameters.Add(new MySqlParameter("@meta_value", ""));
                                datos_seo_6.ExecuteNonQuery();
                            }



                        }
                        conexion.cerrarWP();

                    }


                }


            }
            catch (Exception ex)
            {
                try
                {

                    String sqlWP;
                    DataTable tablaSeo = new DataTable();
                    sqlWP = "SELECT WPM.meta_value as Codp FROM wppostmeta as WPM inner JOIN wpposts as WP on WPM.post_id=WP.ID and meta_key='_sku' WHERE replace(post_name,'/','-')='" + codp.Replace("/","-") + "';";
                    conexion.conectarWP();
                    MySqlDataAdapter datosWP = new MySqlDataAdapter(sqlWP, conexion.conWP);
                    datosWP.Fill(tablaSeo);
                    codp = tablaSeo.Rows[0]["Codp"].ToString();
                    conexion.cerrarWP();
                }
                catch (Exception ex2)
                {

                }

            }

            //**************************************


            int retorno = 0;
            string mensaje_error = "";
            int exite_producto = 0;
            int temp = 0;
            bool entero = false;
            if (codp == null)
            {
                entero = false;
            }
            else
            {
                entero = int.TryParse(codp, out temp);
                if (codp.IndexOf("-") != -1)
                {
                    entero = false;
                }
            }

            if (entero)
            {
                List<FotosModels> listado_fotos = new List<FotosModels>();
                dynamic va = "";
                try
                {
                    string cadena = HttpContext.Request.Url.AbsoluteUri;
                    var p = HttpContext.Request.Url.Host;

                    ViewData["HostUrl"] = HttpContext.Request.Url.Host;
                    ViewData["titulodescripcion"] = "";
                    ViewData["descripcion"] = "";
                    ViewData["titulovideo"] = "";
                    ViewData["video1"] = "";
                    ViewData["video2"] = "";

                    // String sql;

                    string codigo_p = "";
                    string nombre_p = "";
                    int portada_p = 0;
                    long referencia_p = 0;
                    //byte[] foto = null;
                    string ruta = "";


                    sql = "SELECT productos.codigo,productos.nombre,productos.unidad,IFNULL(productos.observaciones,'') AS observaciones,productos.pesogrs,";
                    sql += "productos.precio,";
                    sql += "productos.agotado,";
                    sql += "productos.preciooferta,productos.venceoferta,";
                    sql += "CASE ";
                    sql += "WHEN productos.oferta=1 AND productos.venceoferta >= NOW() THEN 1 ";
                    sql += "ELSE 0 ";
                    sql += "END ";
                    sql += "AS ofertavigente,";
                    sql += "(SELECT ruta  ";
                    sql += "FROM fotos ";
                    sql += "WHERE producto=productos.codigo ORDER BY portada DESC limit 1) AS ruta,";
                    sql += "CAST(productos.indcabezaesquelon AS SIGNED) AS indcabezaesquelon,";
                    sql += "CAST(productos.presentacion AS SIGNED) AS presentacion,";
                    sql += "CAST(productos.inddescripcion AS SIGNED) AS inddescripcion,";
                    sql += "productos.costo,";
                    sql += "CASE ";
                    sql += "WHEN productos.cantidaddesde=0 THEN 1 ";
                    sql += "ELSE productos.cantidaddesde ";
                    sql += "END ";
                    sql += "AS cantidaddesde ";
                    sql += "FROM productos ";
                    sql += "WHERE productos.codigo=@codigo";
                    conexion.conectar();


                    using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                    {

                        cmd.Parameters.AddWithValue("@codigo", codp);

                        using (MySqlDataReader d = cmd.ExecuteReader())
                        {
                            while (d.Read())
                            {
                                exite_producto = 1;
                                int ofertavigente = Convert.ToInt32(d["ofertavigente"].ToString());
                                double precio = 0;
                                if (ofertavigente == 1)
                                {
                                    precio = Convert.ToDouble(d["preciooferta"].ToString());
                                }
                                else
                                {
                                    precio = Convert.ToDouble(d["precio"].ToString());
                                }
                                ViewData["codigo"] = Convert.ToInt32(d["codigo"].ToString());
                                ViewData["nombre"] = d["nombre"].ToString();
                                ViewData["unidad"] = d["unidad"].ToString();
                                ViewData["pesogrs"] = d["pesogrs"].ToString();
                                ViewData["precio"] = precio;
                                ViewData["agotado"] = Convert.ToInt32(d["agotado"].ToString());
                                ViewData["preciooferta"] = Convert.ToDouble(d["preciooferta"].ToString());
                                ViewData["preciooriginal"] = Convert.ToDouble(d["precio"].ToString());
                                ViewData["ofertavigente"] = ofertavigente;
                                ViewData["observaciones"] = d["observaciones"].ToString();
                                ViewData["inddescripcion"] = Convert.ToDouble(d["inddescripcion"].ToString());
                                ViewData["indpresentacion"] = Convert.ToDouble(d["presentacion"].ToString());
                                ViewData["indcabezaesquelon"] = Convert.ToDouble(d["indcabezaesquelon"].ToString());
                                ViewData["costo"] = Convert.ToDouble(d["costo"].ToString());
                                ViewData["cantidaddesde"] = Convert.ToDouble(d["cantidaddesde"].ToString());

                            }
                        }
                    }


                    sql = "SELECT producto,nombre,portada,referencia,ruta  ";
                    sql += "FROM fotos ";
                    sql += "WHERE producto=@producto ORDER BY portada DESC";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                    {

                        cmd.Parameters.AddWithValue("@producto", codp);

                        using (MySqlDataReader d = cmd.ExecuteReader())
                        {
                            while (d.Read())
                            {
                                ruta = d["ruta"].ToString();
                                if (ruta == "")
                                {
                                    ruta = "~/img/imagen_sin_foto.png";
                                }
                                codigo_p = d["producto"].ToString();
                                nombre_p = d["nombre"].ToString();
                                referencia_p = Convert.ToInt64(d["referencia"].ToString());
                                portada_p = Convert.ToInt32(d["portada"].ToString());
                                //foto= (byte[])d["foto"];
                                listado_fotos.Add(new FotosModels
                                {
                                    producto = codp,
                                    nombre = nombre_p,
                                    referencia = referencia_p,
                                    portada = portada_p,
                                    rutafoto = ruta
                                    //foto=foto

                                });

                            }
                        }
                    }



                    //**************  SEO ***************
                    String sqlWP;
                    DataTable tablaSeo = new DataTable();
                    sqlWP = "call ProductoSEO (" + codp + ", 'https://www.fishboxchile.cl/Productos/', 'https://www.fishboxchile.cl/" + ruta + "')";
                    conexion.conectarWP();
                    MySqlDataAdapter datosWP = new MySqlDataAdapter(sqlWP, conexion.conWP);
                    datosWP.Fill(tablaSeo);
                    conexion.cerrarWP();

                    if (tablaSeo.Rows.Count > 0)
                    {
                        if (tablaSeo.Rows[0]["post_title"].ToString() != "")
                        {
                            ViewData["nombre"] = tablaSeo.Rows[0]["post_title"].ToString();
                        }
                        if (tablaSeo.Rows[0]["post_excerpt"].ToString() != "")
                        {
                            ViewData["observaciones"] = tablaSeo.Rows[0]["post_excerpt"].ToString();
                        }
                        ViewData["descripcionSEO"] = tablaSeo.Rows[0]["post_content"].ToString();
                        ViewData["ScriptSEO"] = tablaSeo.Rows[0]["CODIGO1"].ToString();
                        ViewData["altimage"] = tablaSeo.Rows[0]["altimage"].ToString();
                        ViewData["SLUG"] = tablaSeo.Rows[0]["post_name"].ToString();
                        ViewData["header"] = tablaSeo.Rows[0]["Header"].ToString();
                    }
                    else
                    {
                        ViewData["descripcionSEO"] = "";
                        ViewData["ScriptSEO"] = "";
                        ViewData["altimage"] = "";
                        ViewData["SLUG"] = "";
                        ViewData["header"] = "";
                    }


                    //var url = "http://acrilicosylaminados.com/ayl2/";
                    //var textFromFile = (new System.Net.WebClient()).DownloadString(url);

                    //var dataFile = Server.MapPath("~/Views/HtmlProductos/prueba.cshtml");
                    //System.IO.File.WriteAllText(@dataFile, textFromFile);


                    //************** FIN SEO ***************




                    sql = "SELECT titulodescripcion,descripcion,titulovideo,codigovideo1,codigovideo2 FROM productos WHERE codigo=" + codp + "";

                    using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                    {
                        using (MySqlDataReader d = cmd.ExecuteReader())
                        {
                            if (d.Read())
                            {

                                ViewData["titulodescripcion"] = d["titulodescripcion"].ToString();
                                ViewData["descripcion"] = d["descripcion"].ToString();
                                ViewData["titulovideo"] = d["titulovideo"].ToString();
                                ViewData["video1"] = d["codigovideo1"].ToString();
                                ViewData["video2"] = d["codigovideo2"].ToString();
                            }
                            else
                            {

                            }
                        }
                    }
                    retorno = 1;
                    if (exite_producto == 0)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View(listado_fotos);
                    }
                    //return View("~/Views/HtmlProductos/prueba.cshtml");

                }
                catch (Exception error)
                {
                    retorno = -1;
                    mensaje_error = error.Message.ToString();
                    return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
                }
                finally
                {
                    conexion.cerrar();
                }

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public ActionResult partialDetalleProducto(ProductosModels producto)
        {
            int retorno = 0;
            string mensaje_error = "";
            List<FotosModels> listado_fotos = new List<FotosModels>();
            dynamic va = "";
            try
            {
                ViewData["codigo"] = producto.codigo;
                ViewData["nombre"] = producto.nombre;
                ViewData["unidad"] = producto.unidad;
                ViewData["pesogrs"] = producto.pesogrs;
                ViewData["precio"] = producto.precio;
                ViewData["agotado"] = producto.agotado;
                ViewData["preciooferta"] = producto.preciooferta;
                ViewData["preciooriginal"] = producto.preciooriginal;
                ViewData["ofertavigente"] = producto.ofertavigente;
                ViewData["observaciones"] = producto.observaciones;
                ViewData["inddescripcion"] = producto.inddescripcion;
                ViewData["indpresentacion"] = producto.indpresentacion;
                ViewData["indcabezaesquelon"] = producto.indcabezaesquelon;
                ViewData["presentacion"] = producto.presentacion;
                ViewData["costo"] = producto.costo;

                ViewData["titulodescripcion"] = "";
                ViewData["descripcion"] = "";
                ViewData["titulovideo"] = "";
                ViewData["video1"] = "";
                ViewData["video2"] = "";

                String sql;

                sql = "SELECT producto,nombre,portada,referencia,ruta  ";
                sql += "FROM fotos ";
                sql += "WHERE producto=@producto ORDER BY portada DESC";
                string codigo_p = "";
                string nombre_p = "";
                int portada_p = 0;
                long referencia_p = 0;
                //byte[] foto = null;
                string ruta = "";
                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {

                    cmd.Parameters.AddWithValue("@producto", producto.codigo);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            ruta = d["ruta"].ToString();
                            if (ruta == "")
                            {
                                ruta = "~/img/imagen_sin_foto.png";
                            }
                            codigo_p = d["producto"].ToString();
                            nombre_p = d["nombre"].ToString();
                            referencia_p = Convert.ToInt64(d["referencia"].ToString());
                            portada_p = Convert.ToInt32(d["portada"].ToString());
                            //foto= (byte[])d["foto"];
                            listado_fotos.Add(new FotosModels
                            {
                                producto = producto.codigo.ToString(),
                                nombre = nombre_p,
                                referencia = referencia_p,
                                portada = portada_p,
                                rutafoto = ruta
                                //foto=foto

                            });

                        }
                    }
                }
                sql = "SELECT titulodescripcion,descripcion,titulovideo,codigovideo1,codigovideo2 FROM productos WHERE codigo=" + producto.codigo + "";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {

                            ViewData["titulodescripcion"] = d["titulodescripcion"].ToString();
                            ViewData["descripcion"] = d["descripcion"].ToString();
                            ViewData["titulovideo"] = d["titulovideo"].ToString();
                            ViewData["video1"] = d["codigovideo1"].ToString();
                            ViewData["video2"] = d["codigovideo2"].ToString();
                        }
                        else
                        {

                        }
                    }
                }
                retorno = 1;
                return View(listado_fotos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }


        }

        public ActionResult partialListadoCarrito(List<ProductosModels> carrito, double contador, int valorFaltaGratis)
        {


            ViewData["contador"] = contador;
            ViewData["valorFaltaGratis"] = valorFaltaGratis;
            Session["carrito"] = carrito;
            Session["contador"] = contador;
            return View(carrito);
        }
        public ActionResult partialListadoProductosOfertas(string fila)
        {
            List<ProductosModels> ListaProductos = new List<ProductosModels>();

            DataTable dt_productos = new DataTable();
            int retorno = 0;
            string mensaje_error = "";
            try
            {
                int indpresentacion = 0;
                int indcabezaesquelon = 0;
                int inddescripcion = 0;
                String sql;
                string rutfoto = "";
                sql = "SELECT productos.codigo,productos.nombre,productos.unidad,IFNULL(productos.observaciones,'') AS observaciones,productos.pesogrs,";
                sql += "productos.precio,";
                sql += "productos.agotado,";
                sql += "productos.preciooferta,";
                sql += "DATE_FORMAT(productos.venceoferta,'%d/%m/%Y') AS venceoferta,";
                sql += "(SELECT ruta  ";
                sql += "FROM fotos ";
                sql += "WHERE producto=productos.codigo ORDER BY portada DESC limit 1) AS ruta,";
                sql += "CAST(productos.indcabezaesquelon AS SIGNED) AS indcabezaesquelon,";
                sql += "CAST(productos.presentacion AS SIGNED) AS presentacion,";
                sql += "CAST(productos.inddescripcion AS SIGNED) AS inddescripcion,";
                sql += "productos.costo,";
                sql += "CASE ";
                sql += "WHEN productos.cantidaddesde=0 THEN 1 ";
                sql += "ELSE productos.cantidaddesde ";
                sql += "END ";
                sql += "AS cantidaddesde ";
                sql += "FROM productos ";
                sql += "WHERE productos.oferta=@oferta AND productos.venceoferta>=NOW() AND productos.indpagweb=@indpagweb AND productos.codigo NOT IN (405,406) AND productos.agotado=0 AND productos.nuevooc=0 ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                datos.SelectCommand.Parameters.AddWithValue("@oferta", "1");
                datos.SelectCommand.Parameters.AddWithValue("@indpagweb", "1");

                datos.Fill(dt_productos);

                foreach (DataRow datos_tabla in dt_productos.Rows)
                {
                    rutfoto = datos_tabla["ruta"].ToString();
                    if (rutfoto == "")
                    {
                        rutfoto = "~/img/imagen_sin_foto.png";
                    }
                    indpresentacion = Convert.ToInt32(datos_tabla["presentacion"].ToString());
                    indcabezaesquelon = Convert.ToInt32(datos_tabla["indcabezaesquelon"].ToString());
                    inddescripcion = Convert.ToInt32(datos_tabla["inddescripcion"].ToString());
                    ListaProductos.Add(new ProductosModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                        unidad = datos_tabla[2].ToString(),
                        observaciones = datos_tabla[3].ToString(),
                        pesogrs = datos_tabla[4].ToString(),
                        precio = Convert.ToInt32(datos_tabla[5]),
                        agotado = Convert.ToInt32(datos_tabla[6]),
                        preciooferta = Convert.ToDouble(datos_tabla[7]),
                        venceoferta = datos_tabla[8].ToString(),
                        rutafoto = rutfoto,
                        indpresentacion = indpresentacion,
                        indcabezaesquelon = indcabezaesquelon,
                        inddescripcion = inddescripcion,
                        costo = Convert.ToInt32(datos_tabla["costo"].ToString()),
                        cantidaddesde = Convert.ToDouble(datos_tabla["cantidaddesde"])

                    });
                }
                ViewData["fila_padre"] = fila;
                retorno = 1;
                return View(ListaProductos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }
        public ActionResult partialListadoProductosXfamilia(string familia, string fila)
        {
            List<ProductosModels> ListaProductos = new List<ProductosModels>();
            Session["ss_familia"] = familia;
            DataTable dt_productos = new DataTable();
            int retorno = 0;
            string mensaje_error = "";
            try
            {
                int indpresentacion = 0;
                int indcabezaesquelon = 0;
                int inddescripcion = 0;
                String sql;
                string rutfoto = "";
                sql = "SELECT productos.codigo,productos.nombre,productos.unidad,IFNULL(productos.observaciones,'') AS observaciones,productos.pesogrs,";
                sql += "productos.precio,";
                sql += "productos.agotado,";
                sql += "productos.preciooferta,productos.venceoferta,";
                sql += "CASE ";
                sql += "WHEN productos.oferta=1 AND productos.venceoferta >= NOW() THEN 1 ";
                sql += "ELSE 0 ";
                sql += "END ";
                sql += "AS ofertavigente,";
                sql += "(SELECT ruta  ";
                sql += "FROM fotos ";
                sql += "WHERE producto=productos.codigo ORDER BY portada DESC limit 1) AS ruta,";
                sql += "CAST(productos.indcabezaesquelon AS SIGNED) AS indcabezaesquelon,";
                sql += "CAST(productos.presentacion AS SIGNED) AS presentacion,";
                sql += "CAST(productos.inddescripcion AS SIGNED) AS inddescripcion,";
                sql += "productos.costo,";
                sql += "CASE ";
                sql += "WHEN productos.cantidaddesde=0 THEN 1 ";
                sql += "ELSE productos.cantidaddesde ";
                sql += "END ";
                sql += "AS cantidaddesde ";
                sql += "FROM productos ";
                sql += "WHERE productos.familia=@familia AND productos.indpagweb=@indpagweb AND productos.codigo NOT IN (405,406) AND productos.nuevooc=0 ";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                datos.SelectCommand.Parameters.AddWithValue("@familia", familia);
                datos.SelectCommand.Parameters.AddWithValue("@indpagweb", "1");
                datos.Fill(dt_productos);

                foreach (DataRow datos_tabla in dt_productos.Rows)
                {
                    rutfoto = datos_tabla[10].ToString();
                    indpresentacion = Convert.ToInt32(datos_tabla["presentacion"].ToString());
                    indcabezaesquelon = Convert.ToInt32(datos_tabla["indcabezaesquelon"].ToString());
                    inddescripcion = Convert.ToInt32(datos_tabla["inddescripcion"].ToString());
                    if (rutfoto == "")
                    {
                        rutfoto = "~/img/imagen_sin_foto.png";
                    }
                    ListaProductos.Add(new ProductosModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                        unidad = datos_tabla[2].ToString(),
                        observaciones = datos_tabla[3].ToString(),
                        pesogrs = datos_tabla[4].ToString(),
                        precio = Convert.ToInt32(datos_tabla[5]),
                        agotado = Convert.ToInt32(datos_tabla[6]),
                        preciooferta = Convert.ToDouble(datos_tabla[7]),
                        venceoferta = datos_tabla[8].ToString(),
                        ofertavigente = Convert.ToInt32(datos_tabla[9]),
                        rutafoto = rutfoto,
                        indpresentacion = indpresentacion,
                        indcabezaesquelon = indcabezaesquelon,
                        inddescripcion = inddescripcion,
                        costo = Convert.ToInt32(datos_tabla["costo"].ToString()),
                        cantidaddesde = Convert.ToDouble(datos_tabla["cantidaddesde"])


                    });
                }
                ViewData["fila_padre"] = fila;
                retorno = 1;
                return View(ListaProductos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }


        public ActionResult partialFamilias_continua(string nombre)
        {
            List<FamiliasModels> ListaFamilias = new List<FamiliasModels>();

            DataTable dt_productos = new DataTable();
            int retorno = 0;
            int indicar_filtro = 0;
            string mensaje_error = "";
            try
            {
                String sql;
                sql = "SELECT f.codigo,f.nombre,f.foto ";
                sql += "FROM familias f ";
                if (nombre != "")
                {
                    sql += "LEFT JOIN productos p ON f.codigo=p.familia ";
                    sql += "WHERE p.nombre like @nombre GROUP BY p.familia";
                    indicar_filtro = 1;
                }
                else
                {
                    sql += "ORDER BY f.orden ";
                }

                //if (Session["ss_familia"] == null)
                //{

                //    sql = "SELECT f.codigo,f.nombre,f.foto ";
                //    sql += "FROM familias f ";
                //    if (nombre != "")
                //    {
                //        sql += "LEFT JOIN productos p ON f.codigo=p.familia ";
                //        sql += "WHERE p.nombre like @nombre GROUP BY p.familia";
                //        indicar_filtro = 1;
                //    }
                //    else
                //    {
                //        sql += "ORDER BY f.orden ";
                //    }
                //}
                //else
                //{
                //    sql = "SELECT f.codigo,f.nombre,f.foto ";
                //    sql += "FROM familias f WHERE f.codigo="+ Session["ss_familia"].ToString();
                //    indicar_filtro = 1;

                //}
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                if (nombre != "")
                {
                    datos.SelectCommand.Parameters.AddWithValue("@nombre", "%" + nombre + "%");

                }
                datos.Fill(dt_productos);
                byte[] imagen = null;

                foreach (DataRow datos_tabla in dt_productos.Rows)
                {
                    string f = datos_tabla["foto"].ToString();
                    if (f == "")
                    {
                        var folder = Server.MapPath("~/img/ver_familia.png");
                        //extension = "image/png";
                        imagen = System.IO.File.ReadAllBytes(folder);
                    }
                    else
                    {
                        imagen = (byte[])datos_tabla["foto"];
                    }
                    ListaFamilias.Add(new FamiliasModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                        foto = imagen

                    });
                }
                ViewData["indicar_filtro"] = indicar_filtro;
                retorno = 1;
                return View("partialFamilias", ListaFamilias);
                //return RedirectToAction("partialFamilias", ListaFamilias);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

        public ActionResult partialFamilias(string nombre)
        {
            List<FamiliasModels> ListaFamilias = new List<FamiliasModels>();

            DataTable dt_productos = new DataTable();
            int retorno = 0;
            int indicar_filtro = 0;
            string mensaje_error = "";
            try
            {
                String sql;
                Session["ss_familia"] = null;
                sql = "SELECT f.codigo,f.nombre,f.foto ";
                sql += "FROM familias f ";
                if (nombre != "")
                {
                    sql += "LEFT JOIN productos p ON f.codigo=p.familia ";
                    sql += "WHERE p.nombre like @nombre GROUP BY p.familia";
                    indicar_filtro = 1;
                }
                else
                {
                    sql += "ORDER BY f.orden ";

                }
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                if (nombre != "")
                {
                    datos.SelectCommand.Parameters.AddWithValue("@nombre", "%" + nombre + "%");

                }
                datos.Fill(dt_productos);
                byte[] imagen = null;

                foreach (DataRow datos_tabla in dt_productos.Rows)
                {
                    string f = datos_tabla["foto"].ToString();
                    if (f == "")
                    {
                        var folder = Server.MapPath("~/img/ver_familia.png");
                        //extension = "image/png";
                        imagen = System.IO.File.ReadAllBytes(folder);
                    }
                    else
                    {
                        imagen = (byte[])datos_tabla["foto"];
                    }
                    ListaFamilias.Add(new FamiliasModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                        foto = imagen

                    });
                }
                ViewData["indicar_filtro"] = indicar_filtro;
                retorno = 1;
                return View(ListaFamilias);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

        public JsonResult GetPresentacion(string codigopre)
        {
            List<OrdenModels> orden = new List<OrdenModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre FROM presentaciones WHERE codigopre=" + codigopre;
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    orden.Add(new OrdenModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(orden, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrden()
        {
            List<OrdenModels> orden = new List<OrdenModels>();

            DataTable lista_tabla = new DataTable();
            try
            {
                String sql = "";

                sql = "SELECT codigo,nombre,referencia FROM ordenamiento";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                datos.Fill(lista_tabla);
                foreach (DataRow datos_tabla in lista_tabla.Rows)
                {
                    orden.Add(new OrdenModels
                    {
                        codigo = datos_tabla[0].ToString(),
                        nombre = datos_tabla[1].ToString(),
                    });
                }
            }
            catch (Exception error)
            {
                string errorMsj = error.Message.ToString();
            }
            return Json(orden, JsonRequestBehavior.AllowGet);
        }


    }
}