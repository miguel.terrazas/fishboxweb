﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;

namespace WebFBX.Controllers
{
    public class ClientesController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();


        private int valida_Cliente_repetido(string telefono)
        {
            int retorno = 0;
            DataTable dt_rut = new DataTable();
            String sql = "SELECT telefono FROM clientes WHERE telefono='" + telefono.Trim() + "'";
            conexion.conectar();
            MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
            conexion.cerrar();
            datos.Fill(dt_rut);
            if (dt_rut.Rows.Count > 0)
            {
                retorno = 1;
            }
            return retorno;
        }
        [HttpPost]
        public ActionResult GuardarCliente(string rutcliente, String razonsocial, string direccion, string ciudad, string comuna, string telefonos, String email, string condominio, string coddesc)
        {
            int retorno = 0;
            int existe_cliente = 0;
            try
            {
                telefonos = telefonos.Replace(" ", "");
                string rut = "0";
                string dv = ".";
                if (rutcliente != "")
                {
                    if (rutcliente.IndexOf("-") != -1)
                    {
                        rut = rutcliente.Split('-')[0];
                        dv = rutcliente.Split('-')[1];
                    }
                }

                existe_cliente = valida_Cliente_repetido(telefonos);
                string anotacionescall = ".";
                if (existe_cliente == 0)
                {
                    conexion.conectar();
                    String sqlcliente = "INSERT INTO clientes (rut,dv,nombre,direccion,ciudad,comuna,telefono,email,condominio,anotacionescall) " +
                    "VALUES(" + rut + ",'" + dv + "','" + razonsocial + "','" + direccion + "','" + ciudad + "','" + comuna + "','" + telefonos + "','" + email + "','" + condominio + "','" + anotacionescall + "')";
                    MySqlCommand datoscliente = new MySqlCommand(sqlcliente, conexion.con);
                    datoscliente.ExecuteScalar();
                    retorno = 1;
                }
                else
                {
                    conexion.conectar();
                    String sql = "UPDATE clientes SET " +
                    "rut = " + rut + "," +
                    "dv = '" + dv + "'," +
                    "nombre = '" + razonsocial + "'," +
                    "direccion = '" + direccion + "'," +
                    "ciudad = '" + ciudad + "'," +
                    "comuna = '" + comuna + "'," +
                    "email = '" + email + "'," +
                    "condominio = '" + condominio + "' " +
                    "WHERE telefono ='" + telefonos+"'";
                    MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                    datos.ExecuteScalar();
                    retorno = 1;
                }
                if (coddesc != "")
                {
                    String start = "start transaction";
                    MySqlCommand datosST = new MySqlCommand(start, conexion.con);
                    datosST.ExecuteScalar();


                    string sql = "SELECT coddesc FROM descuentotelefonos WHERE coddesc=@codigo AND telefono=@telefono LIMIT 1 FOR UPDATE";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                    {
                        cmd.Parameters.AddWithValue("@codigo", coddesc);
                        cmd.Parameters.AddWithValue("@telefono", telefonos);

                        using (MySqlDataReader d = cmd.ExecuteReader())
                        {
                            if (d.Read())
                            {
                                retorno = -2;
                            }
                            else
                            {

                            }
                        }
                    }
                    if (retorno == 1)
                    {
                        int cantidad = 0;
                        int consumido = 0;
                        sql = "SELECT valor,cantidad,consumido FROM descuentos WHERE coddesc=@codigo LIMIT 1 FOR UPDATE";
                        using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                        {
                            cmd.Parameters.AddWithValue("@codigo", coddesc);
                            using (MySqlDataReader d = cmd.ExecuteReader())
                            {
                                if (d.Read())
                                {

                                    cantidad = Convert.ToInt32(d["cantidad"].ToString());
                                    consumido = Convert.ToInt32(d["consumido"].ToString());
                                    if (cantidad > consumido)
                                    {
                                        retorno = 1;

                                    }
                                    else
                                    {
                                        retorno = -3;
                                    }
                                }
                                else
                                {

                                }
                            }
                        }
                    }
                    if (retorno == 1)
                    {
                        String sql_insert = "INSERT descuentotelefonos SET coddesc=@coddesc,telefono=@telefono";
                        MySqlCommand datos_insert = new MySqlCommand(sql_insert, conexion.con);
                        datos_insert.Parameters.AddWithValue("@coddesc", coddesc);
                        datos_insert.Parameters.AddWithValue("@telefono", telefonos);
                        datos_insert.ExecuteNonQuery();

                        String sql_update = "UPDATE descuentos SET consumido=(consumido+1) WHERE coddesc=@coddesc";
                        MySqlCommand datos_update = new MySqlCommand(sql_update, conexion.con);
                        datos_update.Parameters.AddWithValue("@coddesc", coddesc);
                        datos_update.ExecuteNonQuery();
                    }


                    String commit = "commit";
                    MySqlCommand datosCommit = new MySqlCommand(commit, conexion.con);
                    datosCommit.ExecuteScalar();
                }


                return Json(retorno);

            }
            catch (Exception ex)
            {
                if (coddesc != "")
                {
                    String rollback = "rollback";
                    MySqlCommand datosRollback = new MySqlCommand(rollback, conexion.con);
                    datosRollback.ExecuteScalar();
                }
                return Json(ex.Message);
            }
            finally
            {
                conexion.cerrar();
            }
        }



        [HttpPost]
        public ActionResult LeerCliente(string telefono)
        {
            try
            {
                string sql = "SELECT CAST(CONCAT(rut, '-', dv) AS CHAR(11)) AS rutcompleto,nombre,direccion,ciudad,comuna,giro,telefono,email,condominio,rut FROM clientes WHERE telefono='" + telefono.Trim() + "' limit 1";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                conexion.cerrar();
                DataTable dt = new DataTable();
                datos.Fill(dt);
                string rut = "";
                String rznsocial = "";
                String direccion = "";
                String ciudad = "";
                String comuna = "";
                String giro = "";
                string telefonos = "";
                String email = "";
                string condominio = "";

                int ok = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    ok = 1;
                    if (dr[9].ToString() == "0")
                    {
                        rut = "";
                    }
                    else
                    {
                        rut = dr[0].ToString();
                    }
                    rznsocial = dr[1].ToString();
                    direccion = dr[2].ToString();
                    ciudad = dr[3].ToString();
                    comuna = dr[4].ToString();
                    giro = dr[5].ToString();
                    telefonos = dr[6].ToString();
                    email = dr[7].ToString();
                    condominio = dr[8].ToString();

                }
                return Json(new { okjson = ok, rutjson = rut, rznsocialjson = rznsocial, direccionjson = direccion, ciudadjson = ciudad, comunajson = comuna, girojson = giro, telefonosjson = telefonos, emailjson = email, condominiojson = condominio });
            }
            catch (Exception errores)
            {
                int ok = -1;
                return Json(new { okjson = ok, mensajejson = errores.Message });
            }
        }
    }
}