﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;

namespace WebFBX.Controllers
{
    public class AdminParametrosController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();
       

        public ActionResult Aindex()
        {
            ViewData["datos"] = "jala";
            conexion.conectar();
            string sql = "SELECT concat('$',format(compraminima,2,'de_DE')) as compraminima FROM PARAMETROS;";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        ViewData["compraMinima"] = d["compraminima"].ToString();
                    }
                }
            }
            conexion.cerrar();
            return View();
        }

        [HttpPost]
        public ActionResult ActualizaX()
        {
            conexion.conectar();
            string sql = "update compraminima FROM PARAMETROS;";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        ViewData["compraMinima"] = d["compraminima"].ToString();
                    }
                }
            }
            conexion.cerrar();
            
            return Json(1);
        }

        public JsonResult Actualiza(double carrito, string contador)
        {
            conexion.conectar();
            string sql = "update parametros set compraminima=@valor;";
            MySqlCommand daotsUpdate = new MySqlCommand(sql, conexion.con);
            daotsUpdate.Parameters.Add(new MySqlParameter("@valor", contador.Replace(".","").Replace("$","").Replace(" ","")));
            daotsUpdate.ExecuteNonQuery();

            conexion.cerrar();
            return Json(1);
        }

    }
}
