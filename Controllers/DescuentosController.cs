﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;

namespace WebFBX.Controllers
{
    public class DescuentosController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();


        [HttpPost]
        public ActionResult BuscarDescuento(string codigo,string telefono,int total)
        {
            int retorno = 0;
            int cantidad = 0;
            int consumido = 0;
            int tipo = 0;
            try
            {
                conexion.conectar();

                string sql = "SELECT coddesc FROM descuentotelefonos WHERE coddesc=@codigo AND telefono=@telefono LIMIT 1";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    cmd.Parameters.AddWithValue("@codigo", codigo);
                    cmd.Parameters.AddWithValue("@telefono", telefono);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            retorno = -2;
                        }
                        else
                        {

                        }
                    }
                }
                if (retorno == 0)
                {
                    sql = "SELECT valor,cantidad,consumido,tipo FROM descuentos WHERE coddesc=@codigo LIMIT 1";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                    {
                        cmd.Parameters.AddWithValue("@codigo", codigo);
                        using (MySqlDataReader d = cmd.ExecuteReader())
                        {
                            if (d.Read())
                            {

                                cantidad = Convert.ToInt32(d["cantidad"].ToString());
                                tipo = Convert.ToInt32(d["tipo"].ToString());
                                consumido = Convert.ToInt32(d["consumido"].ToString());
                                if (cantidad > consumido)
                                {
                                    if (tipo == 0)
                                    {
                                        retorno = Convert.ToInt32(d["valor"].ToString());

                                    }
                                    else
                                    {
                                        double porcentaje= Convert.ToDouble(d["valor"].ToString());
                                        double descuento= (porcentaje * total) / 100;
                                        retorno = (int)Math.Round(descuento,0);
                                    }
                                    Session["codigo_descuento"] = codigo;
                                }
                                else
                                {
                                    retorno = -3;
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                }
                return Json(retorno);

            }
            catch (Exception ex)
            {

                return Json(ex.Message);
            }
            finally
            {
                conexion.cerrar();
            }
        }
        [HttpPost]
        public ActionResult EditarDescuentoValor(string codigo, string campo, string tipo,string valor_input)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE descuentos ";
                sql += "SET " + campo + " = @campo, tipo= @tipo ";
                sql += "WHERE coddesc = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@campo", valor_input));
                datos.Parameters.Add(new MySqlParameter("@tipo", tipo));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult EditarDescuento(string codigo, string campo, string valor_input)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                string sql = "UPDATE descuentos ";
                sql += "SET " + campo + " = @campo ";
                sql += "WHERE coddesc = @codigo ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@campo", valor_input));
                datos.Parameters.Add(new MySqlParameter("@codigo", codigo));
                datos.ExecuteNonQuery();

                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        public ActionResult partialListadoDescuentos()
        {

            List<DescuentosModels> Listadescuentos = new List<DescuentosModels>();

            int retorno = 0;
            string mensaje_error = "";
            string codigo = "";
            int cantidad = 0;
            double valor = 0;
            int consumido = 0;

            try
            {
                String sql;

                sql = "SELECT coddesc,tipo,cantidad,valor,consumido";
                sql += " FROM descuentos ";

                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            codigo = d["coddesc"].ToString();
                            cantidad = Convert.ToInt32(d["cantidad"]);
                            valor = Convert.ToDouble(d["valor"]);
                            consumido = Convert.ToInt32(d["consumido"]);
                            Listadescuentos.Add(new DescuentosModels
                            {
                                codigo = codigo,
                                cantidad = cantidad,
                                valor = valor,
                                tipo = Convert.ToInt32(d["tipo"]),
                                consumido = consumido
                            });
                        }
                    }
                }
                retorno = 1;
                return View(Listadescuentos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }

        public ActionResult ActualizarDescuentos()
        {
            if (Session["ss_UserCodigo"] == null)
            {
                Session["UserName"] = string.Empty;
                Session.Abandon();
                Session.Clear();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult GuardarDescuento(DescuentosModels descuentos)
        {
            var retorno = "0";
            try
            {
                conexion.conectar();

                string sql = "INSERT descuentos ";
                sql += "SET coddesc = @coddesc";
                sql += ",cantidad = @cantidad";
                sql += ",tipo = @tipo";
                sql += ",valor = @valor";
                sql += ",consumido = @consumido";


                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@coddesc", descuentos.codigo));
                datos.Parameters.Add(new MySqlParameter("@cantidad", descuentos.cantidad));
                datos.Parameters.Add(new MySqlParameter("@tipo", descuentos.tipo));
                datos.Parameters.Add(new MySqlParameter("@valor", descuentos.valor));
                datos.Parameters.Add(new MySqlParameter("@consumido", descuentos.consumido));
                datos.ExecuteNonQuery();
                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }
        [HttpPost]
        public ActionResult EliminarDescuento(FormCollection form_)
        {
            var retorno = 0;
            try
            {
                String coddesc = form_["codigo"];
                string sql = "DELETE FROM descuentos WHERE coddesc=@codigo";
                conexion.conectar();
                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@codigo", coddesc));
                datos.ExecuteNonQuery();
                conexion.cerrar();
                retorno = 1;
                return Json(retorno);
            }
            catch (Exception error)
            {

                return Json(error.Message);
            }

        }
    }
}