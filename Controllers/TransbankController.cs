﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transbank.Webpay;
using System.Web.UI;
using MySql.Data.MySqlClient;
using System.Data;
using WebFBX.Models;
using WebFBX.Clases;
using Fluentx.Mvc;
using System.Text;

namespace WebPayVDN.Controllers
{
    public class TransbankController : Controller
    {
        private Dictionary<string, string> request = new Dictionary<string, string>();
        Dictionary<string, object> objData = new Dictionary<string, object>();

        // GET: Transbank
        Clase_Conexion conexion = new Clase_Conexion();

        public string presentacion(int codigo)
        {
            string sql = "SELECT nombre FROM presentaciones WHERE codigo=@codigo";
            string nombre = "";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);

                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {

                        nombre = d["nombre"].ToString();
                    }
                    else
                    {

                    }
                }
            }
            return nombre;
        }

        public ActionResult TransbankNormal(DatosClientesModels datos_clienes_obj, List<ProductosModels> carrito)
        {
            //****************** MODO PRUEBA *********************
            var configuration = Configuration.ForTestingWebpayPlusNormal();

            //****************** MODO OFICIAL *********************
            //var configuration = new Configuration();
            //configuration.Environment = "PRODUCCION";
            //configuration.CommerceCode = "597035710582";
            //configuration.PrivateCertPfxPath = Server.MapPath("~/Certificado/5970_vdn_tbk.pfx");
            //configuration.Password = "597035710582vdn";
            string linea = "0";

            /** Creacion Objeto Webpay */
            var transaction = new Webpay(configuration);

            string httpHost = Request.ServerVariables["HTTP_HOST"].ToString();
            string selfURL = Request.ServerVariables["URL"].ToString();

            /** Información de Host para crear URL */
            ViewData["httpHost"] = httpHost;
            ViewData["selfURL"] = selfURL;


            string action = !String.IsNullOrEmpty(Request.QueryString["action"]) ? Request.QueryString["action"] : "init";

            /** Crea URL de Aplicación */
            string sample_baseurl = "http://" + httpHost + selfURL;

            /** Crea Dictionary con descripción */
            var description = new Dictionary<string, string>
            {
                { "VD", "Venta Deb&iacute;to" },
                { "VN", "Venta Normal" },
                { "VC", "Venta en cuotas" },
                { "SI", "cuotas sin inter&eacute;s" },
                { "S2", "2 cuotas sin inter&eacute;s" },
                { "NC", "N cuotas sin inter&eacute;s" }
            };

            /** Crea Dictionary con codigos de resultado */
            var codes = new Dictionary<string, string>
            {
                { "0", "Transacci&oacute;n aprobada" },
                { "-1", "Rechazo de transacci&oacute;n" },
                { "-2", "Transacci&oacute;n debe reintentarse" },
                { "-3", "Error en transacci&oacute;n" },
                { "-4", "Rechazo de transacci&oacute;n" },
                { "-5", "Rechazo por error de tasa" },
                { "-6", "Excede cupo m&aacute;ximo mensual" },
                { "-7", "Excede l&iacute;mite diario por transacci&oacute;n" },
                { "-8", "Rubro no autorizado" }
            };

            //HttpContext.Current.Response.Write("<p style='font-weight: bold; font-size: 200%;'>Ejemplos Webpay - Transacci&oacute;n Normal</p>");
            string buyOrder;
            string tx_step = "";

            switch (action)
            {
                default:
                    tx_step = "Init";

                    try
                    {
                        var random = new Random();

                        /** Monto de la transacción */
                        decimal amount = Convert.ToDecimal(datos_clienes_obj.total);

                        /** Orden de compra de la tienda */
                        //buyOrder = random.Next(0, 1000).ToString();
                        long[] retorno_ocweb = NroOrdenWeb();

                        if (retorno_ocweb[0] == -1)
                        {
                            return Json(new { retornoJson = -3, mensajeJson = "Error al obtener N° Orden de Compra" });
                        }
                        else
                        {
                            buyOrder = retorno_ocweb[1].ToString();
                            
                            /** (Opcional) Identificador de sesión, uso interno de comercio */
                            string sessionId = random.Next(0, 1000).ToString();

                            /** URL Final */
                            string urlReturn = sample_baseurl + "?action=result";

                            /** URL Final */
                            string urlFinal = sample_baseurl + "?action=end";

                            request.Add("amount", amount.ToString());
                            request.Add("buyOrder", buyOrder.ToString());
                            request.Add("sessionId", sessionId.ToString());
                            request.Add("urlReturn", urlReturn.ToString());
                            request.Add("urlFinal", urlFinal.ToString());

                            /** Ejecutamos metodo initTransaction desde Libreria */
                            var result = transaction.NormalTransaction.initTransaction(amount, buyOrder, sessionId, urlReturn, urlFinal);

                            /** Verificamos respuesta de inicio en webpay */
                            if (result.token != null && result.token != "")
                            {
                                //message = "Sesion iniciada con exito en Webpay";                            
                            }
                            else
                            {
                                //message = "webpay no disponible";
                                return View();
                            }

                            new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request);
                            new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(result);

                            Session.Add("ssresult_url", result.url);
                            Session.Add("ssresult_token", result.token);

                            ViewData["result_url"] = (string)(Session["ssresult_url"]);
                            ViewData["result_token"] = (string)(Session["ssresult_token"]);  

                            string[] retorno_pedido = GuardarPedidoTemp(datos_clienes_obj, carrito, buyOrder.ToString(), result.token);

                            if (retorno_pedido[0] == "-1")
                            {
                                return RedirectToAction("TransbankErrorConexion", "Transbank", new { tipo_guardar = "Datos temporales", error = retorno_pedido[1], error_ordenweb = buyOrder.ToString(), error_token = result.token });
                            }
                            else if (retorno_pedido[0] == "-2")
                            {
                                return Json(new { retornoJson = -2, mensajeJson = retorno_pedido[1] });
                            }
                            else
                            {
                                objData.Add("token_ws", result.token);
                                return this.RedirectAndPost(result.url, objData);
                                //return View();
                            }

                            //HttpContext.Current.Response.Write("<form action=" + result.url + " method='post'><input type='hidden' name='token_ws' value=" + result.token + "><input type='submit' value='Continuar &raquo;'></form>");
                        }
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("TransbankError", "Transbank", new { error = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request) });
                    }

                case "result":
                    tx_step = "Get Result";
                    try
                    {
                        //HttpContext.Current.Response.Write("<p style='font-weight: bold; font-size: 150%;'>Step: " + tx_step + "</p>");

                        /** Obtiene Información POST */
                        string[] keysPost = Request.Form.AllKeys;
                        linea = "1";
                        /** Token de la transacción */
                        string token = (string)(Session["ssresult_token"]); //Request.Form["token_ws"];
                        //string ddd= (string)(Session["ssresult_token"]);
                        linea = "2";

                        request.Add("token", token.ToString());
                        linea = "3";

                        var result = transaction.NormalTransaction.getTransactionResult(token);
                        linea = "4";

                        new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request);
                        linea = "5";
                        new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(result);
                        linea = "6";

                        if (result.detailOutput[0].responseCode == 0)
                        {
                            //message = "Pago ACEPTADO por webpay (se deben guardar datos para mostrar voucher)";
                            ViewData["authorizationCode"] = result.detailOutput[0].authorizationCode;
                            ViewData["commercecode"] = result.detailOutput[0].commerceCode;
                            ViewData["amount"] = result.detailOutput[0].amount;
                            ViewData["buyOrder"] = result.detailOutput[0].buyOrder;
                            linea = "7";
                            string[] retorno_pedido = GuardarPedido(result.detailOutput[0].buyOrder, token);
                            linea = "8";
                            if (retorno_pedido[0] == "-1")
                            {
                                return RedirectToAction("TransbankErrorConexion", "Transbank", new { tipo_guardar = "Datos oficiales", error = retorno_pedido[1], error_ordenweb = result.detailOutput[0].buyOrder, error_token = token });
                            }
                            else
                            {

                                objData.Add("token_ws", token);
                                ViewData["result_urlRedirection"] = result.urlRedirection;
                                ViewData["token"] = token;

                                return this.RedirectAndPost(result.urlRedirection, objData);
                            }
                        }
                        else
                        {
                            ViewData["result_urlRedirection"] = result.urlRedirection;
                            ViewData["token"] = token;
                            ViewData["authorizationCode"] = result.detailOutput[0].authorizationCode;
                            ViewData["commercecode"] = result.detailOutput[0].commerceCode;
                            ViewData["amount"] = result.detailOutput[0].amount;
                            ViewData["buyOrder"] = result.detailOutput[0].buyOrder;
                            ViewData["fechayhora"] = result.transactionDate;
                            return RedirectToAction("TransbankError", "Transbank", new { error = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request) });
                            //return View();

                            //message = "Pago RECHAZADO por webpay [Codigo]=> " + result.detailOutput[0].responseCode + " [Descripcion]=> " + codes[result.detailOutput[0].responseCode.ToString()];
                        }

                    }
                    catch (Exception ex)
                    {
                        var contentType = "text/plain";
                        byte[] bytes = Encoding.ASCII.GetBytes(ex.Message + " LINEA:" + linea);
                        return File(bytes, contentType, "error.txt");
                        //return RedirectToAction("TransbankError", "Transbank", new { error = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request) });
                    }

                case "end":
                    tx_step = "End";
                    var oc_anulado = "";

                    try
                    {
                        new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request);

                        if (Request.Form["token_ws"] != null)
                        {
                            request.Add("", "");
                            new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(Request.Form["token_ws"]);
                            //message = "Transacci&oacute;n Finalizada";

                            string next_page = sample_baseurl + "?action=nullify";

                            //ViewData["authorizationCode"] = result.detailOutput[0].authorizationCode;
                            //ViewData["commercecode"] = result.detailOutput[0].commerceCode;
                            //ViewData["amount"] = result.detailOutput[0].amount;
                            //ViewData["buyOrder"] = result.detailOutput[0].buyOrder;

                        }
                        else if (Request.Form["TBK_TOKEN"] != null)
                        {
                            foreach (string key in Request.Form.AllKeys)
                            {
                                //HttpContext.Current.Response.Write("[" + key + "] = " + Request.Form[key] + "<br>");
                                if (key == "TBK_TOKEN")
                                {
                                    oc_anulado = Request.Form[key];
                                    Session.Add("ssoc_anulado", oc_anulado);
                                }
                            }
                        }

                        ViewData["tbk_anulada"] = (string)(Session["ssoc_anulado"]); ;
                        return RedirectToAction("Index", "Home");
                    }
                    catch (Exception ex)
                    {
                        //var contentType = "text/plain";
                        //byte[] bytes = Encoding.ASCII.GetBytes(ex.Message);
                        //return File(bytes, contentType, "error2.txt");
                        return RedirectToAction("TransbankError", "Transbank", new { error = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request) });
                    }

                case "nullify":
                    tx_step = "nullify";

                    try
                    {
                        /** Obtiene Información POST */
                        string[] keysNullify = Request.Form.AllKeys;

                        /** Codigo de Comercio */
                        string commercecode = Request.Form["commercecode"];

                        /** Código de autorización de la transacción que se requiere anular */
                        string authorizationCode = Request.Form["authorizationCode"];

                        /** Monto autorizado de la transacción que se requiere anular */
                        decimal authorizedAmount = Int64.Parse(Request.Form["amount"]);

                        /** Orden de compra de la transacción que se requiere anular */
                        buyOrder = Request.Form["buyOrder"];

                        /** Monto que se desea anular de la transacción */
                        decimal nullifyAmount = 3;

                        request.Add("authorizationCode", authorizationCode.ToString());
                        request.Add("authorizedAmount", authorizedAmount.ToString());
                        request.Add("buyOrder", buyOrder.ToString());
                        request.Add("nullifyAmount", nullifyAmount.ToString());
                        request.Add("commercecode", commercecode.ToString());

                        var resultNullify = transaction.NullifyTransaction.nullify(authorizationCode, authorizedAmount, buyOrder, nullifyAmount, commercecode);
                        new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request);
                        new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultNullify);

                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("TransbankError", "Transbank", new { error = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(request) });
                    }
                    break;
            }

            return View();
        }

        public ActionResult TransbankError(string error)
        {
            ViewData["transbank_error"] = error;
            return View();
        }

        public ActionResult TransbankErrorConexion(string tipo_guardar, string error, string error_ordenweb, string error_token)
        {
            ViewData["tipo_guardar"] = tipo_guardar;
            ViewData["transbank_error"] = "Error interno: " + error;
            ViewData["transbank_ordenweb"] = "N° Pedido: " + error_ordenweb;
            ViewData["transbank_token"] = "Token: " + error_token;
            return View();
        }
        private double[] buscarStockProducto(string codigo, double cantidad)
        {
            DataTable dt_productos = new DataTable();
            int retorno = 0;

            String sql;
            String bodega = "1";
            double stock = 0;

            sql = "SELECT saldo FROM stocks ";
            sql += "WHERE stocks.codigo=@codigo AND stocks.saldo<@cantidad AND bodega=@bodega FOR UPDATE";
            MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
            datos.SelectCommand.Parameters.AddWithValue("@bodega", bodega);
            datos.SelectCommand.Parameters.AddWithValue("@codigo", codigo);
            datos.SelectCommand.Parameters.AddWithValue("@cantidad", cantidad);
            datos.Fill(dt_productos);

            foreach (DataRow datos_tabla in dt_productos.Rows)
            {
                retorno = -2;
                stock = Convert.ToDouble(datos_tabla[0]);
            }
            double[] array = new double[2];
            array[0] = retorno;
            array[1] = stock;
            return array;
        }
        public int obtieneConsumidoDescuento(string codigo)
        {
            string sql = "SELECT consumido FROM descuentos WHERE coddesc=@codigo";
            int consumido = 0;
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        consumido = Convert.ToInt32(d["consumido"]);
                    }
                    else
                    {

                    }
                }
            }
            return consumido;
        }
        public string[] GuardarPedidoTemp(DatosClientesModels datos_clienes_obj, List<ProductosModels> carrito, string ordenweb, string token)
        {
            int retorno = 0;
            String error_msj = "";
            //double pctiva = 19;
            //string nombre_producto = "";

            try
            {
                DataTable lista_tabla = new DataTable();
                DataTable tablaFolio = new DataTable();
                DataTable tablaFolio2 = new DataTable();
                DateTime fecha_dt = DateTime.Now;

                int pesototal = 0;
                string fecha = (fecha_dt).ToString("yyyy-MM-dd");
                conexion.conectar();
                String start = "start transaction";
                MySqlCommand datosST = new MySqlCommand(start, conexion.con);
                datosST.ExecuteScalar();

                if (Session["codigo_descuento"] != null)
                {
                    string codigo_desc = Session["codigo_descuento"].ToString();
                    int consumido = obtieneConsumidoDescuento(codigo_desc);
                    consumido = consumido + 1;
                    String sql_consumido_descuento = " UPDATE descuentos";
                    sql_consumido_descuento += " SET consumido =@consumido ";
                    sql_consumido_descuento += " WHERE coddesc =@coddesc,";
                    MySqlCommand datosDesc = new MySqlCommand(sql_consumido_descuento, conexion.con);
                    datosDesc.Parameters.Add(new MySqlParameter("@consumido", consumido));
                    datosDesc.Parameters.Add(new MySqlParameter("@coddesc", codigo_desc));
                    datosDesc.ExecuteNonQuery();
                }

                String sqlPedido = " INSERT pedidostemp";
                sqlPedido += " SET fecha =@fecha,";
                sqlPedido += " telefono =@telefono,";
                sqlPedido += " cliente =@cliente,";
                sqlPedido += " direccionenvio =@direccionenvio,";
                sqlPedido += " comunaenvio =@comunaenvio,";
                sqlPedido += " condominioenvio =@condominioenvio,";
                sqlPedido += " am =@am,";
                sqlPedido += " pm =@pm,";
                sqlPedido += " vendedor =@vendedor,";
                sqlPedido += " pesototal =@pesototal,";
                sqlPedido += " total =@total,";
                sqlPedido += " web =@web,";
                sqlPedido += " ordenweb =@ordenweb,";
                sqlPedido += " token =@token,";
                sqlPedido += " indpago =@indpago";

                string nombre = "";
                string direccion = "";
                string comuna = "";

                if (datos_clienes_obj.inddirecciondiferente == 1)
                {
                    nombre = datos_clienes_obj.nombredirefente;
                    direccion = datos_clienes_obj.direcciondirefente;
                    comuna = datos_clienes_obj.comunadiferente;
                }
                else
                {
                    nombre = datos_clienes_obj.nombre;
                    direccion = datos_clienes_obj.direccion;
                    comuna = datos_clienes_obj.comuna;
                }
                MySqlCommand datosNV = new MySqlCommand(sqlPedido, conexion.con);
                datosNV.Parameters.Add(new MySqlParameter("@fecha", fecha));
                datosNV.Parameters.Add(new MySqlParameter("@telefono", datos_clienes_obj.telefono));
                datosNV.Parameters.Add(new MySqlParameter("@cliente", nombre));
                datosNV.Parameters.Add(new MySqlParameter("@direccionenvio", direccion));
                datosNV.Parameters.Add(new MySqlParameter("@comunaenvio", comuna));
                datosNV.Parameters.Add(new MySqlParameter("@condominioenvio", datos_clienes_obj.condominio));
                datosNV.Parameters.Add(new MySqlParameter("@am", datos_clienes_obj.am));
                datosNV.Parameters.Add(new MySqlParameter("@pm", datos_clienes_obj.pm));
                datosNV.Parameters.Add(new MySqlParameter("@vendedor", datos_clienes_obj.vendedor));
                datosNV.Parameters.Add(new MySqlParameter("@pesototal", datos_clienes_obj.pesototal));
                datosNV.Parameters.Add(new MySqlParameter("@total", datos_clienes_obj.total));
                datosNV.Parameters.Add(new MySqlParameter("@web", 1));
                datosNV.Parameters.Add(new MySqlParameter("@ordenweb", ordenweb));
                datosNV.Parameters.Add(new MySqlParameter("@token", token));
                datosNV.Parameters.Add(new MySqlParameter("@indpago", 2));//2 indica que pagó con transbank

                datosNV.ExecuteNonQuery();

                if (datosNV.LastInsertedId != 0) datosNV.Parameters.Add(new MySqlParameter("registro", datosNV.LastInsertedId));
                int lastId_registro = Convert.ToInt32(datosNV.Parameters["@registro"].Value);

                int nroitem = 0;
                int existe_stock = 0;
                //double cantidad_stock = 0;
                //double precio_det = 0;
                foreach (var detalle in carrito)
                {
                    //double[] retorno_sctock = buscarStockProducto(detalle.codigo, detalle.cantidad);
                    //nombre_producto = detalle.nombre;
                    //existe_stock = Convert.ToInt32(retorno_sctock[0]);
                    //cantidad_stock = retorno_sctock[1];
                    //if (existe_stock == -2)
                    //{
                    //    error_msj = "La cantidad " + detalle.cantidad.ToString("N0") + " del producto: " + nombre_producto + ", supera el stock " + cantidad_stock.ToString("N0");
                    //    break;
                    //}

                    nroitem += 1;
                    String sqlPedidoD = " INSERT pedidosdtemp";
                    sqlPedidoD += " SET registro =@registro,";
                    sqlPedidoD += " fecha =@fecha,";
                    sqlPedidoD += " telefono =@telefono,";
                    sqlPedidoD += " cliente =@cliente,";
                    sqlPedidoD += " codigo =@codigo,";
                    sqlPedidoD += " cantidad =@cantidad,";
                    sqlPedidoD += " unidad =@unidad,";
                    sqlPedidoD += " detalle =@detalle,";
                    sqlPedidoD += " precio =@precio,";
                    sqlPedidoD += " valor =@valor,";
                    sqlPedidoD += " costo =@costo,";
                    sqlPedidoD += " presentacion =@presentacion,";
                    sqlPedidoD += " esquelon =@esquelon,";
                    sqlPedidoD += " cabeza =@cabeza,";
                    sqlPedidoD += " ordenweb =@ordenweb,";
                    sqlPedidoD += " token =@token";

                    double cantidad = detalle.cantidad;
                    int valor = detalle.valor;
                    if (detalle.unidad == "" || detalle.unidad == null)
                    {
                        detalle.unidad = obtieneUnidad(detalle.codigo);
                    }
                    MySqlCommand datosNVD = new MySqlCommand(sqlPedidoD, conexion.con);
                    datosNVD.Parameters.Add(new MySqlParameter("@registro", lastId_registro));
                    datosNVD.Parameters.Add(new MySqlParameter("@fecha", fecha));
                    datosNVD.Parameters.Add(new MySqlParameter("@telefono", datos_clienes_obj.telefono));
                    datosNVD.Parameters.Add(new MySqlParameter("@cliente", datos_clienes_obj.nombre));
                    datosNVD.Parameters.Add(new MySqlParameter("@codigo", detalle.codigo));
                    datosNVD.Parameters.Add(new MySqlParameter("@cantidad", detalle.cantidad));
                    datosNVD.Parameters.Add(new MySqlParameter("@unidad", detalle.unidad));
                    datosNVD.Parameters.Add(new MySqlParameter("@detalle", detalle.nombre));
                    datosNVD.Parameters.Add(new MySqlParameter("@precio", detalle.precio));
                    datosNVD.Parameters.Add(new MySqlParameter("@valor", valor));
                    datosNVD.Parameters.Add(new MySqlParameter("@costo", detalle.costo));
                    datosNVD.Parameters.Add(new MySqlParameter("@presentacion", detalle.presentacion));
                    datosNVD.Parameters.Add(new MySqlParameter("@esquelon", detalle.esquelon));
                    datosNVD.Parameters.Add(new MySqlParameter("@cabeza", detalle.cabeza));
                    datosNVD.Parameters.Add(new MySqlParameter("@ordenweb", ordenweb));
                    datosNVD.Parameters.Add(new MySqlParameter("@token", token));
                    datosNVD.ExecuteNonQuery();
                }
                if (existe_stock == -2)
                {
                    retorno = existe_stock;
                    String rollback = "rollback";
                    MySqlCommand datosRollback = new MySqlCommand(rollback, conexion.con);
                    datosRollback.ExecuteScalar();
                }
                else
                {
                    retorno = lastId_registro;
                    String commit = "commit";
                    MySqlCommand datosCommit = new MySqlCommand(commit, conexion.con);
                    datosCommit.ExecuteScalar();
                    Session["codigo_descuento"] = null;
                }

            }
            catch (Exception ex)
            {
                String rollback = "rollback";
                MySqlCommand datosRollback = new MySqlCommand(rollback, conexion.con);
                datosRollback.ExecuteScalar();
                error_msj = ex.Message.ToString().Replace("'", "");
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }

            string[] array = new string[3];
            array[0] = retorno.ToString();
            array[1] = error_msj;
            array[2] = datos_clienes_obj.total;

            return array;
        }
        public string obtieneUnidad(string codigo)
        {
            string sql = "SELECT unidad FROM productos WHERE codigo=@codigo";
            string unidad = "Un";
            using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
            {
                cmd.Parameters.AddWithValue("@codigo", codigo);
                using (MySqlDataReader d = cmd.ExecuteReader())
                {
                    if (d.Read())
                    {
                        unidad = d["unidad"].ToString();
                    }
                    else
                    {

                    }
                }
            }
            return unidad;
        }
        public string[] GuardarPedido(string ordenweb, string token)
        {
            int retorno = 0;
            String error_msj = "";
            try
            {
                DataTable tablaFolio = new DataTable();
                DataTable tablaFolio2 = new DataTable();
                DataTable dt_pedidod = new DataTable();


                conexion.conectar();
                String ts = "start transaction";
                MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                datos_ts.ExecuteScalar();

                string email = "prueba.opencode@gmail.com";
                string password = "opencode11";
                string smtp = "smtp.gmail.com";
                int puerto = 25;
                string emailrespuesta = "prueba.opencode@gmail.com";
                string sql = "SELECT email,password,smtp,puerto,emailrespuesta FROM parametros";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {

                            email = d["email"].ToString();
                            password = d["password"].ToString();
                            smtp = d["smtp"].ToString();
                            puerto = Convert.ToInt32(d["puerto"].ToString());
                            emailrespuesta = d["emailrespuesta"].ToString();
                        }
                        else
                        {

                        }
                    }
                }
                DateTime fch = DateTime.Now;
                String sqlPed = " INSERT INTO pedidos(";
                sqlPed += " telefono,";
                sqlPed += " cliente,";
                sqlPed += " direccionenvio,";
                sqlPed += " comunaenvio,";
                sqlPed += " condominioenvio,";
                sqlPed += " am,";
                sqlPed += " pm,";
                sqlPed += " fechaent,";
                sqlPed += " pesototal,";
                sqlPed += " total,";
                sqlPed += " web,";
                sqlPed += " vendedor,";
                sqlPed += " ordenweb,";
                sqlPed += " indpago,";
                sqlPed += " token) ";
                sqlPed += " SELECT ";
                sqlPed += " telefono,cliente,direccionenvio,comunaenvio,condominioenvio,am,pm,"+ fch+",pesototal,total,web,vendedor,ordenweb,indpago,token ";
                sqlPed += " FROM pedidostemp";
                sqlPed += " WHERE pedidostemp.ordenweb=@ordenweb AND pedidostemp.token=@token";

                MySqlCommand datosNV = new MySqlCommand(sqlPed, conexion.con);
                datosNV.Parameters.AddWithValue("@ordenweb", ordenweb);
                datosNV.Parameters.AddWithValue("@token", token);
                datosNV.ExecuteNonQuery();

                if (datosNV.LastInsertedId != 0) datosNV.Parameters.Add(new MySqlParameter("registro", datosNV.LastInsertedId));
                int lastId_registro = Convert.ToInt32(datosNV.Parameters["@registro"].Value);

                String sqlPedidoD = " INSERT INTO pedidosd( ";
                sqlPedidoD += " registro,";
                sqlPedidoD += " fecha,";
                sqlPedidoD += " telefono,";
                sqlPedidoD += " cliente,";
                sqlPedidoD += " codigo,";
                sqlPedidoD += " cantidad,";
                sqlPedidoD += " unidad,";
                sqlPedidoD += " detalle,";
                sqlPedidoD += " precio,";
                sqlPedidoD += " valor,";
                sqlPedidoD += " costo,";
                sqlPedidoD += " presentacion,";
                sqlPedidoD += " esquelon,";
                sqlPedidoD += " cabeza)";
                sqlPedidoD += " SELECT ";
                sqlPedidoD += " " + lastId_registro + ",fecha,telefono,cliente,codigo,cantidad,unidad,detalle,precio,valor,costo,presentacion,esquelon,cabeza ";
                sqlPedidoD += " FROM pedidosdtemp ";
                sqlPedidoD += " WHERE pedidosdtemp.ordenweb=@ordenweb AND pedidosdtemp.token=@token";

                MySqlCommand datosNVD = new MySqlCommand(sqlPedidoD, conexion.con);
                datosNVD.Parameters.AddWithValue("@ordenweb", ordenweb);
                datosNVD.Parameters.AddWithValue("@token", token);
                datosNVD.ExecuteNonQuery();

                sql = " SELECT ";
                sql += " (CONCAT(CONVERT(detalle,CHAR), ' ', (CONCAT(CONVERT(presentacion,CHAR), ' ', CONVERT(cabezaesquelon,CHAR))))) AS detalle,registro,telefono,cliente,codigo,cantidad,unidad,precio,valor,presentacion,cabezaesquelon FROM(";
                sql += " SELECT";
                sql += " p.registro,p.telefono,p.cliente,p.codigo,p.cantidad,p.unidad,p.detalle,p.precio,p.valor,";
                sql += " CASE";
                sql += "    WHEN p.presentacion > 0 THEN (SELECT  (CONCAT('<br />TIPO CORTE: ', ' ', nombre)) FROM presentaciones WHERE codigo=p.presentacion)";
                sql += "    ELSE ''";
                sql += " END AS presentacion,";
                sql += " CASE";
                sql += "    WHEN p.cabeza > 0 AND p.esquelon>0 THEN '<br />CABEZA/EZQUELON'";
                sql += "    WHEN p.cabeza > 0 THEN '<br />CABEZA'";
                sql += "    WHEN p.esquelon>0 THEN '<br />EZQUELON'";
                sql += "    ELSE ''";
                sql += " END AS cabezaesquelon ";
                sql += " FROM pedidosd p WHERE p.registro=@registro) AS pedidosdet";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    cmd.Parameters.AddWithValue("@registro", lastId_registro);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        dt_pedidod.Load(d);

                    }
                }


                String sql_deleted = "DELETE FROM pedidostemp WHERE ordenweb=@ordenweb AND token=@token";
                MySqlCommand datos_deleted = new MySqlCommand(sql_deleted, conexion.con);
                datos_deleted.Parameters.AddWithValue("@ordenweb", ordenweb);
                datos_deleted.Parameters.AddWithValue("@token", token);
                datos_deleted.ExecuteNonQuery();

                String sql_deletedd = "DELETE FROM pedidosdtemp WHERE ordenweb=@ordenweb AND token=@token";
                MySqlCommand datos_deletedd = new MySqlCommand(sql_deletedd, conexion.con);
                datos_deletedd.Parameters.AddWithValue("@ordenweb", ordenweb);
                datos_deletedd.Parameters.AddWithValue("@token", token);
                datos_deletedd.ExecuteNonQuery();

                String commit = "commit";
                MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                datos_commit.ExecuteScalar();
                Session["carrito"] = null;
                Session["contador"] = 0;
                retorno = 1;
            }
            catch (Exception ex)
            {
                String rollback = "rollback";
                MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                datos_rollback.ExecuteScalar();
                error_msj = ex.Message.ToString().Replace("'", "");
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }

            string[] array = new string[2];
            array[0] = retorno.ToString();
            array[1] = error_msj;
            return array;
        }

        private long[] NroOrdenWeb()
        {
            int retorno = 0;
            int folio = 0;
            String error_msj = "";

            try
            {
                DataTable tablaFolio = new DataTable();
                DataTable tablaFolio2 = new DataTable();

                conexion.conectar();
                String ts = "start transaction";
                MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                datos_ts.ExecuteScalar();

                String sql = "SELECT numerodoc FROM correlativos WHERE tipomov='TB' LIMIT 1 FOR UPDATE";
                MySqlDataAdapter datosFolio = new MySqlDataAdapter(sql, conexion.con);
                datosFolio.Fill(tablaFolio);
                foreach (DataRow fila in tablaFolio.Rows)
                {
                    folio = Convert.ToInt32(fila[0].ToString());
                }

                if (tablaFolio.Rows.Count == 0)//en caso que no haya registro, creo uno nuevo para el tipo
                {
                    folio = 1;
                    String sqlIst = " INSERT correlativos SET numerodoc=" + folio + ", tipomov='TB' ";
                    MySqlCommand datosIst = new MySqlCommand(sqlIst, conexion.con);
                    datosIst.ExecuteNonQuery();

                    sql = "SELECT numerodoc FROM correlativos WHERE tipomov='TB' LIMIT 1 FOR UPDATE";
                    MySqlDataAdapter datosFolio2 = new MySqlDataAdapter(sql, conexion.con);
                    datosFolio2.Fill(tablaFolio2);
                    foreach (DataRow fila in tablaFolio2.Rows)
                    {
                        folio = Convert.ToInt32(fila[0].ToString());
                    }
                }
                else//actualizo el numero mas 1
                {
                    folio = folio + 1;
                    String sqlUpd = " UPDATE correlativos SET numerodoc=" + folio + " WHERE tipomov='TB'";
                    MySqlCommand datosUpd = new MySqlCommand(sqlUpd, conexion.con);
                    datosUpd.ExecuteNonQuery();
                }

                String commit = "commit";
                MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                datos_commit.ExecuteScalar();
                retorno = 1;
            }
            catch (Exception ex)
            {
                String rollback = "rollback";
                MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                datos_rollback.ExecuteScalar();
                error_msj = ex.Message.ToString().Replace("'", "");
                retorno = -1;
            }
            finally
            {
                conexion.cerrar();
            }

            long[] array = new long[2];
            array[0] = retorno;
            array[1] = folio;
            return array;
        }

    }
}