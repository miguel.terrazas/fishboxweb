﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFBX.Clases;
using WebFBX.Models;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Net;

namespace WebFBX.Controllers
{
    public class FotosController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        [HttpPost]
        public ActionResult ActualizaIndPortada(string referencia,string codigo)
        {
            string retorno = "0";
            try
            {
                conexion.conectar();
                String ts = "start transaction";
                MySqlCommand datos_ts = new MySqlCommand(ts, conexion.con);
                datos_ts.ExecuteScalar();
                string sql = "UPDATE fotos ";
                sql += "SET portada = @portada ";
                sql += "WHERE producto = @producto ";

                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@producto", codigo));
                datos.Parameters.Add(new MySqlParameter("@portada", "0"));
                datos.ExecuteNonQuery();

                sql = "UPDATE fotos ";
                sql += "SET portada = @portada ";
                sql += "WHERE referencia = @referencia ";

                MySqlCommand datos_2 = new MySqlCommand(sql, conexion.con);
                datos_2.Parameters.Add(new MySqlParameter("@referencia", referencia));
                datos_2.Parameters.Add(new MySqlParameter("@portada", "1"));
                datos_2.ExecuteNonQuery();
                retorno = "1";
                String commit = "commit";
                MySqlCommand datos_commit = new MySqlCommand(commit, conexion.con);
                datos_commit.ExecuteScalar();
            }
            catch (Exception ex)
            {
                String rollback = "rollback";
                MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                datos_rollback.ExecuteScalar();
                retorno = ex.Message.ToString().Replace("'", ""); 
            }
            finally
            {
                conexion.cerrar();
            }
            return Json(retorno);
        }

        [HttpPost]
        public ActionResult GuardarFotos(int producto, string indportada)
        {
            var retorno = 0;
            try
            {
                conexion.conectar();
                string filePath = "";
                string nombre_ruta = "";
                string sql = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    if (file.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        fileName = fileName.Replace(" ", "_");
                        string formato = System.IO.Path.GetExtension(file.FileName);

                        using (BinaryReader reader = new BinaryReader(file.InputStream))
                        {
                            byte[] archivo_binario = reader.ReadBytes(file.ContentLength);

                            var folder = Server.MapPath("~/productos_img/" + producto);

                            sql = "INSERT fotos";
                            sql += " SET foto =@foto";
                            sql += " ,nombre =@nombre";
                            sql += " ,portada =@indportada";
                            sql += " ,formato =@formato";
                            sql += " ,producto =@producto";
                            sql += " ,ruta =@ruta";


                            if (!Directory.Exists(folder))
                            {
                                Directory.CreateDirectory(folder);
                                filePath = Path.Combine(Server.MapPath("~/productos_img/" + producto), fileName);
                                file.SaveAs(filePath);
                            }
                            else
                            {
                                filePath = Path.Combine(Server.MapPath("~/productos_img/" + producto), fileName);
                                file.SaveAs(filePath);

                            }
                            nombre_ruta = "~/productos_img/" + producto + "/" + fileName;

                            MySqlCommand datos_foto = new MySqlCommand(sql, conexion.con);
                            datos_foto.Parameters.Add(new MySqlParameter("@nombre", fileName));
                            datos_foto.Parameters.Add(new MySqlParameter("@foto", archivo_binario));
                            datos_foto.Parameters.Add(new MySqlParameter("@indportada", indportada));
                            datos_foto.Parameters.Add(new MySqlParameter("@formato", formato));
                            datos_foto.Parameters.Add(new MySqlParameter("@producto", producto));
                            datos_foto.Parameters.Add(new MySqlParameter("@ruta", nombre_ruta));

                            datos_foto.ExecuteNonQuery();
                        }
                    }
                }
                //
                sql = "SELECT ";
                sql += "(SELECT ruta  ";
                sql += "FROM fotos ";
                sql += "WHERE producto=Productos.codigo ORDER BY portada DESC limit 1) AS imagenURL ";
                sql += "FROM Productos where codigo=" + producto; //<----------- Pasar codp del artículo modificado

                DataTable tbSeo_0 = new DataTable();
                MySqlDataAdapter datWP_0 = new MySqlDataAdapter(sql, conexion.con);
                datWP_0.Fill(tbSeo_0);
                string imagen = "";
                foreach (DataRow fila_seo in tbSeo_0.Rows)
                {
                    imagen = fila_seo["imagenURL"].ToString().Replace("~", "https://www.fishboxchile.cl/");
                }
                WebClient client = new WebClient();
                Stream stream = client.OpenRead("http://wordpressopencode.cl/wp/descargaImg.php?codigo=" + producto + "&imagen=" + imagen);

                retorno = 1;
                return Json(retorno);
            }
            catch (Exception error)
            {

                return Json(error.Message);
            }
            finally
            {
                conexion.cerrar();
            }

        }

        [HttpPost]
        public ActionResult EliminarFoto(FormCollection form_)
        {
            string retorno = "0";
            try
            {
                string referencia = form_["referencia"];
                string sql = "DELETE FROM fotos WHERE referencia=@referencia";
                conexion.conectar();
                MySqlCommand datos = new MySqlCommand(sql, conexion.con);
                datos.Parameters.Add(new MySqlParameter("@referencia", referencia));
                datos.ExecuteNonQuery();
                conexion.cerrar();
                retorno = "1";
            }
            catch (Exception error)
            {
                retorno = error.Message;
            }
            return Json(retorno);
        }

        public ActionResult GetFoto(int referencia)
        {
            try
            {
                conexion.conectar();
                string sql = "SELECT foto,formato AS extension,nombre FROM fotos WHERE referencia=" + referencia;
                byte[] imagen = null;
                string extension = "";
                string nombre = "foto";
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {
                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        if (d.Read())
                        {
                            imagen = (byte[])d["foto"];
                            extension = (string)d["extension"];
                            //nombre = nombre + referencia.ToString() + "." + extension;
                            nombre= (string)d["nombre"];
                        }
                        else
                        {
                            var folder = Server.MapPath("~/img/imagen_sin_foto.png");
                            string contentType = "image/png";
                            byte[] foto_blanco = System.IO.File.ReadAllBytes(folder);
                            //MySqlConnection.ClearAllPools();
                            return File(foto_blanco, contentType, "imagen.png");
                        }
                    }
                }
                conexion.cerrar();
                //return File(imagen, "image/jpeg");
                return File(imagen, "application/octet-stream", nombre);

            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

        //public FileResult GetFoto(int referencia)
        //{
        //    try
        //    {
        //        using (personal_Entities bd = new personal_Entities())
        //        {
        //            var contentType = "application/pdf";
        //            var pdf = bd.pdfs.Where(x => x.referencia == referencia).FirstOrDefault();
        //            string nombredocumento = "";

        //            if (pdf == null)
        //            {
        //                var folder = Server.MapPath("~/Images/documento_vacio.png");
        //                nombredocumento = "documento_vacio.png";
        //                contentType = "image/png";
        //                byte[] foto_blanco = System.IO.File.ReadAllBytes(folder);
        //                //MySqlConnection.ClearAllPools();

        //                return File(foto_blanco, contentType, nombredocumento);
        //            }
        //            else
        //            {
        //                nombredocumento = pdf.id + ".pdf";
        //                //MySqlConnection.ClearAllPools();

        //                return File(pdf.pdf, contentType, nombredocumento);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var contentType = "text/plain";
        //        byte[] bytes = Encoding.ASCII.GetBytes(ex.Message);
        //        return File(bytes, contentType, "error.txt");

        //    }
        //}
        public ActionResult partialListadoFotos(int codigo)
        {
            int retorno = 0;
            string mensaje_error = "";
            List<FotosModels> listado_fotos = new List<FotosModels>();
            try
            {
                String sql;

                sql = "SELECT producto,nombre,portada,referencia ";
                sql += "FROM fotos ";
                sql += "WHERE producto=@producto ORDER BY portada DESC";
                string codigo_p = "";
                string nombre_p = "";
                int portada_p = 0;
                long referencia_p = 0;

                conexion.conectar();
                using (MySqlCommand cmd = new MySqlCommand(sql, conexion.con))
                {

                    cmd.Parameters.AddWithValue("@producto", codigo);

                    using (MySqlDataReader d = cmd.ExecuteReader())
                    {
                        while (d.Read())
                        {
                            codigo_p = d["producto"].ToString();
                            nombre_p = d["nombre"].ToString();
                            referencia_p = Convert.ToInt64(d["referencia"].ToString());
                            portada_p = Convert.ToInt32(d["portada"].ToString());

                            listado_fotos.Add(new FotosModels
                            {
                                producto = codigo.ToString(),
                                nombre = nombre_p,
                                referencia = referencia_p,
                                portada = portada_p

                            });

                        }
                    }
                }
                retorno = 1;
                return View(listado_fotos);
            }
            catch (Exception error)
            {
                retorno = -1;
                mensaje_error = error.Message.ToString();
                return Json(new { retornoJson = retorno, mensajeJson = mensaje_error });
            }
            finally
            {
                conexion.cerrar();
            }
        }
    }
}