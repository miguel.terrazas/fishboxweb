﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text;
using System.Net;
using WebFBX.Clases;
using MySql.Data.MySqlClient;
using System.Data;

namespace WebFBX.Controllers
{
    public class ContactoController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();

        [HttpPost]
        public ActionResult EnviarMensaje(string nombre, string emailsolicitante, string telefono, string empresa, string mensaje)
        {
            int retorno = 0;
            string errormsj = "";
            try
            {
                string emailenvio = "";
                string passwordenvio = "";
                string host = "";
                int puerto = 25;

                DataTable tabla = new DataTable();

                String sql = "SELECT nombre,email,password,puerto,smtp FROM usuarios WHERE (codigo=@codigo)";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                datos.SelectCommand.Parameters.Add("@codigo", MySqlDbType.VarChar, 100);
                datos.SelectCommand.Parameters["@codigo"].Value = "ADMIN";
                conexion.cerrar();
                datos.Fill(tabla);
                foreach (DataRow user_name in tabla.Rows)
                {
                    emailenvio = user_name[1].ToString();
                    passwordenvio = user_name[2].ToString();
                    puerto = Convert.ToInt32(user_name[3]);
                    host = user_name[4].ToString();
                }


                SmtpClient client = new SmtpClient
                {
                    Host = host,
                    Port = puerto,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailenvio, passwordenvio)
                };
                string titulo_subjet = "";

                string body = "";

                titulo_subjet = "Contacto de cliente";
                body = "<p>Estimado(a) " + "</p>";
                body = body + "<p>Le informamos que  " + nombre + " de la empresa " + empresa + ", con telefono:" + telefono + ", envia el siguiente mensaje: </p>";
                body = body + "<p>" + mensaje + "</p>";
                body = body + "Atte.<br/>CLIENTE CONTACTO FISH BOX<br/>";

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(emailenvio),
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = true,
                    Priority = MailPriority.Normal,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    Subject = titulo_subjet,
                    Body = body,
                };

                mail.To.Add(new MailAddress(emailsolicitante));


                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object s
                        , X509Certificate certificate
                        , X509Chain chain
                        , SslPolicyErrors sslPolicyErrors)
                    { return true; };
                client.Send(mail);
                retorno = 1;

            }
            catch (Exception error)
            {
                //String rollback = "rollback";
                //MySqlCommand datos_rollback = new MySqlCommand(rollback, conexion.con);
                //datos_rollback.ExecuteScalar();
                errormsj = error.Message;
                retorno = -1;
            }
            finally
            {
            }
            return Json(new { retornoJson = retorno, errorJson = errormsj }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ContactoIndex()
        {
            ViewData["HostUrl"] = HttpContext.Request.Url.Host;

            return View();
        }
    }
}