﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebFBX.Clases;

namespace WebFBX.Controllers
{
    public class AccesoController : Controller
    {
        Clase_Conexion conexion = new Clase_Conexion();
        [HttpPost]
        public ActionResult Login(string usuario, string password)
        {
            if (Isvalid(usuario, password) == 1)
            {
                FormsAuthentication.SetAuthCookie(usuario, false);
                return RedirectToAction("VerProductos", "Administracion");
            }
            else
            {
                ViewData["MensajeErrorAcceso"] = "Error: Nombre usuario o contraseña es incorrecta";
                return View("LoguoErrado");
            }
        }
        public ActionResult LoguoErrado()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoguoErrado(string usuario, string password)
        {
            if (Isvalid(usuario, password) == 1)
            {
                FormsAuthentication.SetAuthCookie(usuario, false);
                return RedirectToAction("VerProductos", "Administracion");
            }
            else
            {
                ViewData["MensajeErrorAcceso"] = "Error: Nombre usuario o contraseña es incorrecta";
            }
            return View();
        }

        public int Isvalid(String codigo, String clave)
        {
            var retorno = 0;
            var nombre_usuario = "";
            DataTable tabla = new DataTable();
            try
            {
                String sql = "SELECT nombre,email,password,puerto,smtp FROM usuarios WHERE (codigo=@codigo AND clave=@clave)";
                conexion.conectar();
                MySqlDataAdapter datos = new MySqlDataAdapter(sql, conexion.con);
                datos.SelectCommand.Parameters.Add("@codigo", MySqlDbType.VarChar, 100);
                datos.SelectCommand.Parameters["@codigo"].Value = codigo;
                datos.SelectCommand.Parameters.Add("@clave", MySqlDbType.VarChar, 100);
                datos.SelectCommand.Parameters["@clave"].Value = clave;
                conexion.cerrar();
                datos.Fill(tabla);
                foreach (DataRow user_name in tabla.Rows)
                {
                    nombre_usuario = user_name[0].ToString();
                    retorno = 1;
                    Session["ss_UserCodigo"] = codigo;
                    Session["ss_email"] = user_name[1].ToString();
                    Session["ss_password"] = user_name[2].ToString();
                    Session["ss_puerto"] = user_name[3].ToString();
                    Session["ss_smtp"] = user_name[4].ToString();

                }


            }
            catch (Exception e)
            {
                retorno = -1;
                string error = e.Message;
            }
            return retorno;
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Index","Home");

        }
    }
}