﻿$(document).ready(function () {
    $('#eliminarfoto').click(function (e) {
        e.preventDefault();
        var codigo = $("#codigo_eliminar_hidden").val();
        var fd = new FormData();
        fd.append("referencia", $("#referenciafoto_eliminar_hidden").val());
        $("#ajax_loader_2").css("display", "");
        $.ajax({
            type: "POST",
            url: '/Fotos/EliminarFoto/',
            data: fd,
            DataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                $("#modal_fotos_eliminar").modal("hide")
                $("#ajax_loader_2").css("display", "none");
                if (data == "1") {
                    recargar_listado_Fotos(codigo);
                    $("#modal_exito_foto").modal("show");
                    $("#exito_modal_msj_foto").html('Datos Eliminados Correctamente');
                } else {
                    $("#modal_error").modal("show");
                    $("#error_modal_msj").html("Error interno:" + result.mensajeJson);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#modal_error").modal("show");
                $("#error_modal_msj").html("Error de Conexión");

            }
        });
    });

    $('#id_form_foto').validationEngine();
    $("#guardarfoto").click(function (e) {
        e.preventDefault();
        if (!$("#id_form_foto").validationEngine('validate')) {
            return false;
        }
        var fd = new FormData();
        $("#ajax_loader_2").css("display", "");

        var salir = 0;

        var producto = $("#codigo_producto_foto").val();
        var indportada = ($("#indportada").prop('checked')) ? 1 : 0;
        var cont = 0;
        listado_table_fotos.$("input.portada").each(function () {
            cont++;
            if ($(this).prop('checked') & (indportada == 1)) {
                $("#modal_error").modal("show");
                $("#error_modal_msj").html("Ya tiene una portada establecida en la fila: " + cont);
                $("#ajax_loader_2").css("display", "none");
                salir = 1;
                return false;
            }

        });
        if (salir == 1) {
            return false;
        }


        if ($('#file_foto').val() != "") {
            var fileSize = $('#file_foto')[0].files[0].size;
            if (fileSize > 9000000) {
                $("#modal_error").modal("show");
                $("#error_modal_msj").html("El archivo debe ser menor a 900 KB");
                $("#ajax_loader_2").css("display", "none");
                return false;
            }
            var archivo = $("#file_foto").val();
            var extensiones = archivo.substring(archivo.lastIndexOf("."));
            if (extensiones != ".png" && extensiones != ".jpg") {
                $("#modal_error").modal("show");
                $("#error_modal_msj").html("Archivo de tipo " + extensiones + " no es válido. Debe ser de tipo jpg,png");
                $("#ajax_loader_2").css("display", "none");
                return false;
            }
            var totalFiles = document.getElementById("file_foto").files.length;
            for (var i = 0; i < totalFiles; i++) {
                var file = document.getElementById("file_foto").files[i];
                fd.append("Document", file);
            }
        } else {
            $("#ajax_loader_2").css("display", "none");
            $("#modal_error").modal("show");
            $("#error_modal_msj").html("Debe adjuntar documento");
            return false;
        }


        fd.append("producto", producto);
        fd.append("indportada", indportada);
        $.ajax({
            type: "POST",
            url: '/Fotos/GuardarFotos/',
            data: fd,
            DataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data == 1) {
                    $("#id_form_foto").each(function () { this.reset(); });
                    recargar_listado_Fotos(producto);
                    $("#modal_exito_foto").modal("show");
                    $("#exito_modal_msj_foto").html('Foto Guardada Correctamente');
                } else {
                    $("#modal_error").modal("show");
                    $("#error_modal_msj").html("Error interno. Indique a soporte la descripción del error: " + data);
                }
                $("#ajax_loader_2").css("display", "none");

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#modal_error").modal("show");
                $("#error_modal_msj").html("Error de Conexión");
            }
        });
    });
    $("#file_foto").change(function () {
        $('#id_form_foto').validationEngine('hideAll');

        filePreview(this);
        $('#modal_foto').addClass('actualiza_scroll');

        setTimeout(function () {
            listado_table_fotos.columns.adjust().draw();
        }, 300);
    });

    $('#modal_fotos_eliminar').on('hidden.bs.modal', function () {
        $('#modal_foto').addClass('actualiza_scroll');
        listado_table_fotos.columns.adjust().draw();

    });
    $('#modal_exito_foto').on('hidden.bs.modal', function () {
        $('#modal_foto').addClass('actualiza_scroll');
        listado_table_fotos.columns.adjust().draw();
    });
    $('#modal_error').on('hidden.bs.modal', function () {
        if (listado_table_fotos != "") {
            listado_table_fotos.columns.adjust().draw();
        }
    });

    //$('#modal_documentoscontrato_pdf').on('shown.bs.modal', function () {
    //    recargar_listado_pdf(rut_sindv);
    //});

});

function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#foto_temp").html("");
            $('#foto_temp + img').remove();
            $('#foto_temp').html('<img class="img-fluid" src="' + e.target.result + '" style="max-width:400px !important;max-height:400px !important"/>');

        }
        reader.readAsDataURL(input.files[0]);
    }
}
