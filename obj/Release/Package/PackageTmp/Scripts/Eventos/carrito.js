﻿$('.has-clear input[type="text"]').on('input propertychange', function () {
    var $this = $(this);
    var visible = Boolean($this.val());
    $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
}).trigger('propertychange');

$('.form-control-clear').click(function () {
    $(this).siblings('input[type="text"]').val('')
        .trigger('propertychange').focus();
});
$(document).on("click", ".btn-add_cart_detalle", function (e) {
    e.preventDefault();
    if ($("#prodcantidad_detalle").val() == "") {
        return false;
    }
    $("#ajax_loader_2").css("display", "");
    var codigo = $(this).data("codigo").toString();
    var nombre = $(this).data("nombre");
    //var marca = $(this).data("marca");
    //var stock = $(this).data("stock");
    var agotado = $(this).data("agotado");
    var unidad = $(this).data("unidad");
    var precio = $(this).data("precio").toString();
    var costo = $(this).data("costo");
    var pesogrs = $(this).data("pesogrs");
    var cantidad = parseFloat($("#prodcantidad_detalle").val().replace(",", '.'));
    var cantidad_desde = parseFloat($("#cantidaddesde").val().replace(",", '.'));

    var indpresentacion = $(this).data("indpresentacion");
    var indcabezaesquelon =  parseInt($(this).data("indcabezaesquelon"));
    var inddescripcion = $(this).data("inddescripcion");
    var presentacion = 0;
    var cabeza = 0;
    var esquelon = 0;
    if (indpresentacion > 0) {
        if (parseInt($("#presentacion_detalle").val()) == 0) {
            $("#modal_error").modal("show");
            $("#error_modal_msj").html("No ha indicado tipo de corte");
            $("#ajax_loader_2").css("display", "none");
            return false;
        }

        presentacion = parseInt($("#presentacion_detalle").val());
    }
    if (indcabezaesquelon == 1) {
        cabeza = ($("#cabeza_detalle").prop('checked')) ? 1 : 0;
        esquelon = ($("#esquelon_detalle").prop('checked')) ? 1 : 0;
        //if (cabeza == 0 & esquelon == 0) {
        //    $("#modal_error").modal("show");
        //    $("#error_modal_msj").html("Debe indicar cabeza/esquelon");
        //    $("#ajax_loader_2").css("display", "none");
        //    return false;
        //}

    }

    debugger;
    var precio = parseInt(precio.replace(/\./g, ''));
    //var pos1 = carrito.map(function (e) { return e.codigo; }).indexOf(codigo);
    if (precio == "0") {
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("El precio del producto " + nombre + " no tiene precio definido");
        $("#ajax_loader_2").css("display", "none");
        return false;
    }
    if (agotado == 1) {
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("El producto " + nombre + " no tiene stock disponible");
        $("#ajax_loader_2").css("display", "none");
        return false;
    }
    var pos = carrito.findIndex(i => i.codigo === codigo);

    if (pos !== -1) {
        var cantidad_en_carro = carrito[pos].cantidad;
        var valor = parseFloat((cantidad_en_carro + cantidad) * precio);
        cantidad_carro = cantidad + cantidad_carro;
        valor = redondear(valor, 0);
        carrito[pos].cantidad = carrito[pos].cantidad + cantidad;
        carrito[pos].cabeza = cabeza;
        carrito[pos].esquelon = esquelon;
        carrito[pos].presentacion = presentacion;
        carrito[pos].valor = valor;
        carrito[pos].cantidaddesde = cantidad_desde;

    } else {
        var valor = redondear(cantidad * precio, 0);
        cantidad_carro = cantidad + cantidad_carro;
        id_indice++;
        var carrito_compras = {
            id_indice: id_indice.toString(),
            codigo: codigo,
            cantidad: cantidad,
            nombre: nombre,
            sinsstock: agotado,
            precio: precio,
            costo: costo,
            unidad: unidad,
            pesogrs: pesogrs,
            presentacion: presentacion,
            cabeza: cabeza,
            esquelon: esquelon,
            indpresentacion: indpresentacion,
            indcabezaesquelon: indcabezaesquelon,
            inddescripcion: inddescripcion,
            cantidaddesde: cantidad_desde,
            valor: valor
        }

        carrito.push(carrito_compras);
    }

    var cart = $('.shopping-cart');
    var imgtodrag = $(this).parent('.item').find("img").eq(0);
    if (imgtodrag) {
        var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000, 'easeInOutExpo');

        //setTimeout(function () {
        //    cart.effect("shake", {
        //        times: 2
        //    }, 200);
        //}, 1500);

        imgclone.animate({
            'width': 0,
            'height': 0
        }, function () {
            $(this).detach()
            $("#cantidad_carrito").html('(' + cantidad_carro.toString().replace(".",",") + ')');
        });
    }
    guardarCarritoSession();

    //$("#cantidad_carrito").html('(' + cantidad_carro + ')');

    $("#ajax_loader_2").css("display", "none");
    //$("#modal_ver_detalle").modal("hide");
    //$("#prodcantidad").val(1);
});

$(document).on("click", ".add-to-cart", function (e) {

    var codigo = $(this).data("codigo").toString();
    var nombre = $(this).data("nombre");
    var unidad = $(this).data("unidad");
    //var marca = $(this).data("marca");
    //var stock = $(this).data("stock");
    var agotado = $(this).data("agotado");
    var precio = $(this).data("precio").toString();
    var costo = $(this).data("costo");
    var pesogrs = $(this).data("pesogrs");
    var cantidad_desde = parseFloat($(this).data("cantidaddesde").toString().replace(",","."));

    var cantidad = cantidad_desde;
    var indpresentacion = $(this).data("indpresentacion");
    var indcabezaesquelon = $(this).data("indcabezaesquelon");
    var inddescripcion = $(this).data("inddescripcion");
    
    var presentacion = 0;
    var cabeza = 0;
    var esquelon = 0;
    var precio = parseInt(precio.replace(/\./g, ''));

    if (indpresentacion > 0) {
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("No ha indicado tipo de corte en el detalle del producto");
        return false;
    }

    $("#ajax_loader_2").css("display", "");
    //var pos1 = carrito.map(function (e) { return e.codigo; }).indexOf(codigo);
    if (precio == "0") {
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("El precio del producto " + nombre + " no tiene precio definido");
        $("#ajax_loader_2").css("display", "none");
        return false;
    }
    if (agotado == 1) {
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("El producto " + nombre + " no tiene stock disponible");
        $("#ajax_loader_2").css("display", "none");
        return false;
    }
    var pos = carrito.findIndex(i => i.codigo === codigo);
    if (pos !== -1) {
        var cantidad_en_carro = carrito[pos].cantidad;
        var valor = parseFloat((cantidad_en_carro + cantidad) * precio);
        valor = redondear(valor, 0);
    } else {
        var valor = redondear(cantidad * precio, 0);
    }


    var cart = $('.shopping-cart');
    var imgtodrag = $(this).parents('.item').find("img").eq(0);
    cantidad_carro = cantidad_carro + cantidad;
    if (imgtodrag) {
        var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000, 'easeInOutExpo');

        //setTimeout(function () {
        //    cart.effect("shake", {
        //        times: 2
        //    }, 200);
        //}, 1500);

        imgclone.animate({
            'width': 0,
            'height': 0
        }, function () {
            $(this).detach()
            $("#cantidad_carrito").html('(' + cantidad_carro.toString().replace(".", ",") + ')');
        });
    }




    if (pos !== -1) {
        //mostrar_msj_error("Código repetido");
        //return false;
        carrito[pos].cantidad = carrito[pos].cantidad + cantidad;
        carrito[pos].valor = valor;

    } else {

        id_indice++;
        var carrito_compras = {
            id_indice: id_indice.toString(),
            codigo: codigo,
            cantidad: cantidad,
            nombre: nombre,
            sinsstock: agotado,
            precio: precio,
            costo: costo,
            unidad: unidad,
            pesogrs: pesogrs,
            presentacion: presentacion,
            cabeza: cabeza,
            esquelon: esquelon,
            indpresentacion: indpresentacion,
            indcabezaesquelon: indcabezaesquelon,
            inddescripcion: inddescripcion,
            cantidaddesde: cantidad_desde,
            valor: valor
        }

        carrito.push(carrito_compras);
    }
    guardarCarritoSession();


    $("#ajax_loader_2").css("display", "none");
    //$("#modal_ver_detalle").modal("hide");
    //$("#prodcantidad").val(1);
});
