﻿
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function (e) {
    e.preventDefault();
    fieldName = $(this).attr('data-field');
    var cantidaddesde = $("#cantidaddesde_edit").val();
    type = $(this).attr('data-type');
    var input = $("input[name='" + fieldName + "']");
    var cant_desde = parseFloat(cantidaddesde);
    var currentVal = cant_desde;// parseInt(input.val());
    var valor_input = parseFloat($('.input-number').val().toString().replace(",", "."));
    var valor_plus = valor_input + cant_desde;
    var valor_minus = valor_input - cant_desde;
    debugger;
    if (!isNaN(currentVal)) {
        if (type == 'minus') {

            if (valor_input > cant_desde) {
                input.val(valor_minus.toString().replace(".", ",")).change();
            }
            if (parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if (type == 'plus') {

            if (currentVal < input.attr('max')) {
                input.val(valor_plus.toString().replace(".", ",")).change();
            }
            if (parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }

});
$('.input-number').focusin(function () {
    $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function () {
    if ($('.input-number').val() == "") {
        return false;
    }

 
    var cantidaddesde = $("#cantidaddesde_edit").val();

    var cantidad = parseFloat($(this).val().replace(",", "."));
    debugger;
    if (cantidad < parseFloat(cantidaddesde)) {
        $('.input-number').val("");
        $("#modal_error").modal("show");
        $("#error_modal_msj").html("La cantidad mímina a ingresar es de: " + cantidaddesde.toString().replace(".", ","));
        $("#ajax_loader_2").css("display", "none");
        return false;
    }

    var precio = parseFloat($("#prodprecio_edit").html().replace('$', '').replace(/\./g, ''));
    debugger;
    var valor = cantidad * precio;
    minValue = parseInt($(this).attr('min'));
    maxValue = parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if (valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if (valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }

    $("#prodvalor_edit").html("$ " + Moneda(valor));

});
