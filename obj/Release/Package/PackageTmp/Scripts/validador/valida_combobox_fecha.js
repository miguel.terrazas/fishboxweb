﻿ahora = new Date();
ahoraDay = ahora.getDate();
ahoraMonth = ahora.getMonth();
ahoraYear = ahora.getYear();

function rellenaAnyos(masAnyos) {
    cadena = "";

    for (i = masAnyos; i > 0; i--) {
        cadena += "<option>";
        cadena += ahoraYear - i;
    }
    return cadena;
}
if (ahoraYear < 2000)
    ahoraYear += 1900;
//function cuantosDias(mes, anyo) {
//    var cuantosDias = 31;
//    if (mes == "Abril" || mes == "Junio" || mes == "Septiembre" || mes == "Noviembre")
//        cuantosDias = 30;
//    if (mes == "Febrero" && (anyo / 4) != Math.floor(anyo / 4))
//        cuantosDias = 28;
//    if (mes == "Febrero" && (anyo / 4) == Math.floor(anyo / 4))
//        cuantosDias = 29;
//    return cuantosDias;
//}
function diasMes() {
    var valorSeleccionadoDia = $('option:selected', '.fechaNacimientoDia').val();
	var mes = $('option:selected', '.fechaNacimientoMes').index();
	var ano = $('option:selected', '.fechaNacimientoAno').text();
	

	
	if (mes == "2"){  
		if (((ano % 4 == 0) && (ano % 100 != 0)) || ((ano % 100 == 0) && (ano % 400 == 0))){
			$('.fechaNacimientoDia option[value="29"]').remove();
			$('.fechaNacimientoDia option[value="30"]').remove(); 
			$('.fechaNacimientoDia option[value="31"]').remove();
			$('.fechaNacimientoDia').append('<option value="29">29</option>');
			if (valorSeleccionadoDia > 29) {
			    $('.fechaNacimientoDia option[value="29"').attr('selected', 'selected');
			} else {
			    $('.fechaNacimientoDia option[value="' + valorSeleccionadoDia + '"').attr('selected', 'selected');
			}
			
		}else{
			$('.fechaNacimientoDia option[value="29"]').remove();
			$('.fechaNacimientoDia option[value="30"]').remove(); 
			$('.fechaNacimientoDia option[value="31"]').remove();
			$('.fechaNacimientoDia option[value="28"').attr('selected', 'selected');
		}
	} 
	else if(mes == "4" || mes == "6" || mes=="9" || mes=="11"){ 
			$('.fechaNacimientoDia option[value="29"]').remove();
			$('.fechaNacimientoDia option[value="30"]').remove(); 
			$('.fechaNacimientoDia option[value="31"]').remove();
			$('.fechaNacimientoDia').append('<option value="29">29</option>');
			$('.fechaNacimientoDia').append('<option value="30">30</option>');
			if (valorSeleccionadoDia > 30) {
			    $('.fechaNacimientoDia option[value="30"').attr('selected', 'selected');
			} else {
			    $('.fechaNacimientoDia option[value="' + valorSeleccionadoDia + '"').attr('selected', 'selected');
			}
			
	}else{  
		$('.fechaNacimientoDia option[value="29"]').remove();
		$('.fechaNacimientoDia option[value="30"]').remove(); 
		$('.fechaNacimientoDia option[value="31"]').remove();
		$('.fechaNacimientoDia').append('<option value="29">29</option>');
		$('.fechaNacimientoDia').append('<option value="30">30</option>');
		$('.fechaNacimientoDia').append('<option value="31">31</option>');		
		$('.fechaNacimientoDia option[value="' + valorSeleccionadoDia + '"').attr('selected', 'selected');
		
	} 
}

//segunda forma de hacerlo

/**
       * definimos las variables que almacenaran los componentes de la fecha actual
       */


/**
* Nestcape Navigator 4x cuenta el anyo a partir de 1900, por lo que es necesario
* sumarle esa cantidad para obtener el anyo actual adecuadamente
**/


/**
* funcion para saber cuantos dias tiene cada mes
*/
//function cuantosDias(mes, anyo) {
//    var cuantosDias = 31;
//    if (mes == "Abril" || mes == "Junio" || mes == "Septiembre" || mes == "Noviembre")
//        cuantosDias = 30;
//    if (mes == "Febrero" && (anyo / 4) != Math.floor(anyo / 4))
//        cuantosDias = 28;
//    if (mes == "Febrero" && (anyo / 4) == Math.floor(anyo / 4))
//        cuantosDias = 29;
//    return cuantosDias;
//}

/**
* una vez que sabemos cuantos dias tiene cada mes
* asignamos dinamicamente este numero al combo de los dias dependiendo 
* del mes que aparezca en el combo de los meses
*/
//function asignaDias() {

//    comboDias = $(".fecha_Nacimiento_Dia");
//    comboMeses = $(".fecha_Nacimiento_Mes").val();   // esto realmente no se necesita
//    comboAnyos = $(".fecha_Nacimiento_Anio").val();  // esto realmente no se necesita

//    dias_index = $('option:selected', '.fecha_Nacimiento_Dia').index()

//    Month = $('option:selected', '.fecha_Nacimiento_Mes').text();
//    Year = $('option:selected', '.fecha_Nacimiento_Anio').text();

//    diasEnMes = cuantosDias(Month, Year);
//    diasAhora = comboDias.find("option").length - 1;  // calcula el numero de dias

//    
//    if (diasAhora > diasEnMes) {
//        comboDias.find("option").each(function () {
//            if ($(this).attr("value") > diasEnMes)
//                $(this).remove();
//        });
//    }
//    if (diasEnMes > diasAhora) {
//        for (i = diasAhora + 1; i <= diasEnMes ; i++) {
//            comboDias.append('<option value="' + i + '">' + i + "</option>");
//        }
//    }

//    if (dias_index < 0)
//        dias_index = 0;
//}
/**
* ahora selecionamos en los combos los valores correspondientes 
* a la fecha actual del sistema
*/
//function ponDia() {
//    comboDias = eval("document.formFecha.seleccionaDia");
//    comboMeses = eval("document.formFecha.seleccionaMes");
//    comboAnyos = eval("document.formFecha.seleccionaAnyo");

//    comboAnyos[0].selected = true;
//    comboMeses[ahoraMonth].selected = true;

//    asignaDias();

//    comboDias[ahoraDay - 1].selected = true;
//}

/**
* esta funcion crea dinamicamente el combo de los anyos, empezando
* por el actual y acabando por el actual+masAnyos
*/

